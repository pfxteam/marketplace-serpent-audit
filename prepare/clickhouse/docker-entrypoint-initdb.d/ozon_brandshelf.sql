-- default.ozon_brandshelf definition

CREATE TABLE IF NOT EXISTS default.ozon_brandshelf
(
    `result_id` UUID,
    `ts` DateTime,
    `date_parsed` Date,
    `kw_or_url` String,
    `client` String,
    `category` String,
    `subcategory` String,
    `brand` String,
    `subbrand` String,
    `bs_product_sku` String,
    `bs_product_link` String,
    `bs_title` String,
    `bs_shop` String,
    `bs_shop_id` String,
    `bs_brand` String,
    `city` Nullable(String),
    `ozon_brandid` Nullable(UInt64),
    `ozon_seller_id` Nullable(UInt64),
    `ozon_stock_count` Nullable(UInt64)
)
ENGINE = ReplacingMergeTree(ts)
PARTITION BY toYYYYMM(date_parsed)
ORDER BY (
    date_parsed,
    result_id
)
SETTINGS index_granularity = 8192;