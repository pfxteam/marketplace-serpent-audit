-- default.sbermarket definition

CREATE TABLE IF NOT EXISTS default.sbermarket
(
    `result_id`            UUID,
    `ts`                   DateTime,
    `date_parsed`          Date,
    `kw_or_url`            String,
    `shop_name`            String,
    `shop_address`         String,
    `position`             UInt8,
    `product_id`           UInt64,
    `product_sku`          UInt32,
    `product_retailer_sku` UInt32,
    `rating`               Float32,
    `review_count`         UInt32,
    `title`                String,
    `link`                 String,
    `image_link`           String,
    `sales_points`         Array(String),
    `price`                Float32,
    `oldprice`             Float32,
    `client`               String,
    `category`             String,
    `subcategory`          String,
    `brand`                String,
    `subbrand`             String,
    `device`               String DEFAULT 'M'
)
ENGINE = ReplacingMergeTree(ts)
PARTITION BY toYYYYMM(date_parsed)
ORDER BY (
    date_parsed,
    result_id,
    shop_name,
    position
)
SETTINGS index_granularity = 8192;