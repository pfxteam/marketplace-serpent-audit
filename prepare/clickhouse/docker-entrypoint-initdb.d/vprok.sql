-- default.vprok definition

CREATE TABLE IF NOT EXISTS default.vprok
(
    `result_id`    UUID,
    `ts`           DateTime,
    `date_parsed`  Date,
    `kw_or_url`    String,
    `position`     UInt8,
    `product_id`   UInt32,
    `title`        String,
    `sales_points` Array(String),
    `rating`       Float32,
    `link`         String,
    `image_link`   String,
    `price`        Float32,
    `oldprice`     Float32,
    `client`       String,
    `category`     String,
    `subcategory`  String,
    `brand`        String,
    `subbrand`     String,
    `device`       String DEFAULT 'M'
)
ENGINE = ReplacingMergeTree(ts)
PARTITION BY toYYYYMM(date_parsed)
ORDER BY (
    date_parsed,
    result_id,
    position
)
SETTINGS index_granularity = 8192;