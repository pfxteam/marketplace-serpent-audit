-- default.lavka definition

CREATE TABLE IF NOT EXISTS default.lavka
(
    `result_id` UUID,
    `ts` DateTime,
    `date_parsed` Date,
    `kw_or_url` String,
    `shop_address` String,
    `position` UInt8,
    `product_id` String,
    `lavka_brand` String,
    `title` String,
    `link` String,
    `image_link` String,
    `price` Float32,
    `oldprice` Float32,
    `client` String,
    `category` String,
    `subcategory` String,
    `brand` String,
    `subbrand` String,
    `status` Bool,
    `stocks` UInt32,
    `device` String DEFAULT 'M'
)
ENGINE = ReplacingMergeTree(ts)
PARTITION BY toYYYYMM(date_parsed)
ORDER BY (
    date_parsed,
    result_id,
    shop_address,
    position
)
SETTINGS index_granularity = 8192;