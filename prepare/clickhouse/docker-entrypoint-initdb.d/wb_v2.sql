-- default.wb_v2 definition

CREATE TABLE IF NOT EXISTS default.wb_v2
(
    `result_id`     UUID,
    `ts`            DateTime,
    `date_parsed`   Date,
    `kw_or_url`     String,
    `position`      UInt8,
    `position_type` String,
    `title`         String,
    `seller`        String,
    `delivery`      String,
    `sales_points`  Array(String),
    `rating`        Float32,
    `rating_count`  UInt32,
    `link`          String,
    `image_link`    String,
    `price`         UInt32,
    `oldprice`      UInt32,
    `client`        String,
    `category`      String,
    `subcategory`   String,
    `brand`         String,
    `subbrand`      String,
    `wb_brand`      Nullable(String)
)
ENGINE = ReplacingMergeTree(ts)
PARTITION BY toYYYYMM(date_parsed)
ORDER BY (
    date_parsed,
    result_id,
    position
)
SETTINGS index_granularity = 8192;