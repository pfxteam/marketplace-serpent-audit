import os

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'marketplace_serpent.settings')

app = Celery('marketplace_serpent')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()

# app.conf.broker_url = 'redis://localhost:6379/0'
# app.conf.result_backend = 'redis://localhost:6379/0'

app.conf.task_default_queue = 'default'
app.conf.task_default_exchange_type = 'direct'
app.conf.task_default_routing_key = 'default'

# app.conf.beat_schedule = {
#     'launch_metro_desktop': {
#         'task': 'MP_SERPENT_metro_desktop',
#         'schedule': crontab(minute=5),
#         # 'args': (x, x),
#     },
#     'launch_vprok_desktop': {
#         'task': 'MP_SERPENT_vprok_desktop',
#         'schedule': crontab(minute=7),
#         # 'args': (x, x),
#     },
#     'launch_lavka_desktop': {
#         'task': 'MP_SERPENT_lavka_desktop',
#         'schedule': crontab(minute=9),
#         # 'args': (x, x),
#     },
#     'launch_sbermarket_desktop': {
#         'task': 'MP_SERPENT_sbermarket_desktop',
#         'schedule': crontab(minute=11),
#         # 'args': (x, x),
#     },
#     'launch_metro_mobile': {
#         'task': 'MP_SERPENT_metro_mobile',
#         'schedule': crontab(minute=0),
#         # 'args': (x, x),
#     },
#     'launch_vprok_mobile': {
#         'task': 'MP_SERPENT_vprok_mobile',
#         'schedule': crontab(minute=1),
#         # 'args': (x, x),
#     },
#     'launch_lavka_mobile': {
#         'task': 'MP_SERPENT_lavka_mobile',
#         'schedule': crontab(minute=2),
#         # 'args': (x, x),
#     },
#     'launch_sbermarket_mobile': {
#         'task': 'MP_SERPENT_sbermarket_mobile',
#         'schedule': crontab(minute=3),
#         # 'args': (x, x),
#     },
# }