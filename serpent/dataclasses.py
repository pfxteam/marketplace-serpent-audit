from dataclasses import dataclass


@dataclass
class ProjectData:
    name: str
    description: str
    crontab: str
    screen: str
    category: str
    subcategory: str
    marketplace: str
    shops: list[int]
    geo_id: int
    is_active: bool
    client_id: int = None
    client_name: str = None
    brandshelf: bool = False

    def __post_init__(self):
        self.geo_id = int(self.geo_id) if self.geo_id else None
        self.client_id = int(self.client_id) if self.client_id else None
        self.brandshelf = bool(self.brandshelf)
        self.is_active = bool(self.is_active)
        if isinstance(self.shops, str):
            self.shops = [int(x) for x in self.shops.strip().split(',') if x]
        elif isinstance(self.shops, list):
            self.shops = [int(x) for x in self.shops if x]


@dataclass
class GeoData:
    name: str
    country: str
    city: str
    street: str
    house: str
    lon: float
    lat: float

    def __post_init__(self):
        self.lon = float(self.lon)
        self.lat = float(self.lat)
