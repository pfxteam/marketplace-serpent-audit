from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, unique=True)),
                ('description', models.CharField(blank=True, max_length=1000)),
                ('is_active', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('changed', models.DateTimeField(auto_now=True)),
                ('kw_limit', models.IntegerField(default=1000)),
            ],
        ),
        migrations.CreateModel(
            name='Geo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, unique=True)),
                ('country', models.CharField(max_length=64)),
                ('city', models.CharField(max_length=64)),
                ('street', models.CharField(max_length=64)),
                ('house', models.CharField(max_length=64)),
                ('lon', models.CharField(max_length=64)),
                ('lat', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='Keyword',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('keyword', models.CharField(max_length=255)),
                ('added', models.DateField(auto_now_add=True)),
                ('is_active', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Marketplace',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
                ('tech_name', models.CharField(max_length=128)),
            ],
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('client_limit', models.IntegerField(default=10, help_text='Limit of Client number')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='profile', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Shop',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
                ('tech_name', models.CharField(max_length=64)),
                ('shop_id', models.CharField(max_length=128)),
                ('marketplace', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='serpent.marketplace')),
            ],
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.CharField(default=uuid.uuid4, max_length=64, unique=True)),
                ('name', models.CharField(max_length=200)),
                ('crontab', models.CharField(default='0 * * * *', max_length=64)),
                ('description', models.CharField(blank=True, max_length=1000)),
                ('is_active', models.BooleanField(default=False)),
                ('category', models.CharField(max_length=256)),
                ('subcategory', models.CharField(max_length=256)),
                ('screen', models.CharField(choices=[('M', 'Mobile'), ('D', 'Desktop'), ('B', 'Desktop & Mobile')], max_length=1)),
                ('brandshelf', models.BooleanField(default=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('changed', models.DateTimeField(auto_now=True)),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='serpent.client')),
                ('geo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='serpent.geo')),
                ('marketplace', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='serpent.marketplace')),
                ('shops', models.ManyToManyField(blank=True, to='serpent.shop')),
            ],
        ),
        migrations.CreateModel(
            name='KeywordGroup',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
                ('description', models.CharField(blank=True, max_length=1024)),
                ('is_active', models.BooleanField(default=False)),
                ('typings', models.CharField(default={}, max_length=4096)),
                ('brand', models.CharField(blank=True, max_length=128)),
                ('subbrand', models.CharField(blank=True, max_length=128)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('changed', models.DateTimeField(auto_now=True)),
                ('keywords', models.ManyToManyField(blank=True, to='serpent.keyword')),
                ('project', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='serpent.project')),
            ],
        ),
        migrations.AddIndex(
            model_name='keyword',
            index=models.Index(fields=['keyword'], name='mp_serpent_keyword_idx'),
        ),
        migrations.AddField(
            model_name='client',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='clients', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='client',
            name='shares',
            field=models.ManyToManyField(blank=True, related_name='shared_clients', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='client',
            name='shares_edit',
            field=models.ManyToManyField(blank=True, related_name='shared_editable_clients', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='project',
            unique_together={('name', 'client')},
        ),
    ]
