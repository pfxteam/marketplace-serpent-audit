from django import forms


class LoginUsersForms(forms.Form):
    login = forms.CharField(label='Login', max_length=255)
    password = forms.CharField(label='Password', widget=forms.PasswordInput())


class GeoCreateForm(forms.Form):
    name = forms.CharField(max_length=64)
    country = forms.CharField(max_length=64)
    city = forms.CharField(max_length=64)
    street = forms.CharField(max_length=256)
    house = forms.CharField(max_length=16)
    lon = forms.FloatField()
    lat = forms.FloatField()


class CreateAndEditKeywordGroupForm(forms.Form):
    kwg_name = forms.CharField(
        label="Keyword group name:",
        max_length=100,
        widget=forms.TextInput(attrs={
            'type': 'text',
            'class': 'form-control',
            'id': 'kwg_name',
            'name': 'kwg_name',
            'placeholder': 'Type a new keyword group name...'

        })
    )
    kwg_keywords = forms.CharField(
        label="Words:",
        widget=forms.Textarea(attrs={
            'class': 'form-control',
            'id': 'kwg_keywords',
            'name': 'kwg_keywords',
            'rows': 7,
            'placeholder': "One keyword - one line.\n\nkeyword1\nkeyword2"
        }),
        help_text="One keyword - one line.\n\nkeyword1\nkeyword2"
    )
