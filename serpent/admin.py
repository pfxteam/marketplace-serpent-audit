from django.contrib import admin

from .models.engine import UserProfile
from .models.serpent import Client, Project, Marketplace, Geo, Shop, Keyword, KeywordGroup


class UserProfileAdmin(admin.ModelAdmin):
    list_display = [
        "user",
        "client_limit",
    ]

    search_fields = [
        "user__username",
        "user__email",
    ]

    list_filter = [
        "client_limit",
    ]


class ClientAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'name',
        'is_active',
        'description',
        'owner',
        'created',
        'changed',
        'kw_limit',
    )

    search_fields = (
        'id',
        'name',
        'description',
    )

    list_filter = (
        'is_active',
    )


class ProjectAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'name',
        'is_active',
        'description',
        'crontab',
        'category',
        'subcategory',
        'screen',
        'marketplace',
        'brandshelf',
        'client',
        'created',
        'changed',
    )

    search_fields = (
        'id',
        'name',
        'description',
        'crontab',
        'category',
        'subcategory',
        'geo__name',
        'client__name',
    )

    list_filter = (
        'is_active',
        'screen',
        'marketplace',
        'brandshelf',
    )


class MarketplaceAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'name',
        'tech_name',
    )

    search_fields = (
        'id',
        'name',
        'tech_name',
    )


class GeoAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'name',
        'country',
        'city',
        'street',
        'house',
        'lon',
        'lat',
    )

    search_fields = (
        'id',
        'name',
        'country',
        'city',
        'street',
        'house',
        'lon',
        'lat',
    )


class ShopAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'name',
        'tech_name',
        'shop_id',
        'marketplace',
    )

    search_fields = (
        'id',
        'name',
        'shop_id',
        'marketplace__id',
        'marketplace__name',
    )


class KeywordAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'keyword',
        'is_active',
        'added',
    )

    search_fields = (
        'id',
        'keyword',
    )

    list_filter = (
        'is_active',
    )


class KeywordGroupAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'name',
        'description',
        'is_active',
        'typings',
        'brand',
        'subbrand',
        'project',
        'created',
        'changed',
    )

    search_fields = (
        'id',
        'name',
        'description',
        'typings',
        'brand',
        'subbrand',
        'project__id',
        'project__name',
    )

    list_filter = (
        'is_active',
    )


admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Marketplace, MarketplaceAdmin)
admin.site.register(Geo, GeoAdmin)
admin.site.register(Shop, ShopAdmin)
admin.site.register(Keyword, KeywordAdmin)
admin.site.register(KeywordGroup, KeywordGroupAdmin)
