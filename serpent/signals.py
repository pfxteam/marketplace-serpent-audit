from json import dumps

from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django_celery_beat.models import PeriodicTask, CrontabSchedule

from .models.serpent import Project


@receiver(post_save, sender=Project)
def update_cron_task_for_project(sender, instance, created, **kwargs):
    minute, hour, day_of_week, day_of_month, month_of_year = instance.crontab.split(" ")
    schedule, schedule_created = CrontabSchedule.objects.get_or_create(
        minute=minute,
        hour=hour,
        day_of_week=day_of_week,
        day_of_month=day_of_month,
        month_of_year=month_of_year,
    )
    if created:
        task_name = instance.uuid
        PeriodicTask.objects.create(
            task="MP_SERPENT_task_generator",
            crontab=schedule,
            name=task_name,
            enabled=True,
            kwargs=dumps({'project': instance.uuid})
        )
    else:
        change = False
        periodic_task = PeriodicTask.objects.get(name=instance.uuid)
        if periodic_task.enabled != instance.is_active:
            periodic_task.enabled = instance.is_active
            change = True
        if schedule != periodic_task.crontab:
            periodic_task.crontab = schedule
            change = True
        if change:
            periodic_task.save()


@receiver(pre_delete, sender=Project)
def delete_cron_task_for_project(sender, instance, **kwargs):
    periodic_tasks = PeriodicTask.objects.filter(name=instance.uuid)
    for periodic_task in periodic_tasks:
        periodic_task.delete()
