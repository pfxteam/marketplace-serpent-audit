import datetime
from random import randint
from time import sleep

from lxml import html
from lxml.html import HtmlElement
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from serpent.parsers.helpers import check_and_get_browser_client, lxml_get_inner_text, clean_string, format_error


def parse(html_data: str):
    print("[F:parse][D:D] Starting work with HTML data.")

    result = []

    xpaths = {
        'items': ['.//main[@id="main-content-id"]//div[contains(@class,"p159305c")]/div'],  # [Element]
        'image_link': ['.//img/@src'],  # str
        'product_id': ['./div/@data-item-id'],  # str
        'link': ['.//a[@data-type="product-card-link"]/@href'],  # str
        'title': [  # Element
            './/h3[contains(@class, "twim5w0")]',
            './/h3'
        ],
        'oldprice': ['.//span[@class="c1jd7nwq"]//span[contains(@class, "t18stym3")]'],  # Element
        'price': [  # Element
            './/span[@class="tmwo625"]//span[contains(@class, "b1g852u4")]',
            './/span[@class="tmwo625"]//span[contains(@class, "t18stym3")]'
        ],

    }
    tree: HtmlElement = html.fromstring(
        html=html_data,
        parser=html.HTMLParser(encoding='utf-8')
    )
    for _xpath in xpaths['items']:
        items = tree.xpath(_xpath)
        if items:
            print(f"[F:parse][D:D] Found {len(items)} items by xpath '{_xpath}'")
            break
    else:
        raise Exception("No items!")

    xpaths.pop('items')
    for position, item in enumerate(items, start=1):
        data = {
            'position': position,
            'status': True,
            'stocks': 0,
        }
        # Fill
        for field in xpaths.keys():
            for _xpath in xpaths[field]:
                res = item.xpath(_xpath)
                if res:
                    if isinstance(res[0], str):
                        data[field] = str(res[0])
                    else:  # It's Element
                        data[field] = clean_string(lxml_get_inner_text(res[0]))
                    break
        # Fix
        for field in ['oldprice', 'price']:
            if data.get(field):
                data[field] = data[field].strip()
                if " " in data[field]:
                    data[field] = data[field].split(' ')[0]
        if not data.get('link'):
            data['link'] = data['product_id']
        data['image_link'] = data['image_link'].replace('-pixelize', '')

        result.append(data)

    print(f"[F:parse][D:D] Work has been finished. Result: {result}")

    return result


def scrap(task: dict, browser: dict, address: dict):
    print(
        f"[F:scrap][D:D] Task data: {task} ({type(task)});\n"
        f"Browser: {browser};\n"
        f"Address data: {address} ({type(address)})"
    )

    def check_status():
        print(f"[F:check_status][D:D] Checking...")
        if (
                ("500" in browser['client'].page_source and "сервер не отвечает" in browser[
                    'client'].page_source.lower())
                or "что-то не так с сервером" in browser['client'].page_source.lower()
                or "попробуйте обновить страницу позже" in browser['client'].page_source.lower()
        ):
            print(f"[F:check_status][D:D] 500 error has been met.")
            return {
                'status': False,
                'message': '500 ERROR'
            }
        print(f"[F:check_status][D:D] All ok.")
        return {
            'status': True
        }

    def check_delivery_address():

        def change_shop_address():
            combobox_input = browser['client'].find_element(By.XPATH, './/input[@type="search" and @role="combobox"]')
            combobox_ok_button = browser['client'].find_element(By.XPATH, './/div[contains(@class, "aw6oc3a")]')
            browser['action'].move_to_element(combobox_input)
            browser['action'].click(combobox_input)
            browser['action'].perform()
            while len(combobox_input.get_attribute('value')):
                combobox_input.send_keys(Keys.BACKSPACE)
                combobox_input.send_keys(Keys.DELETE)
            browser['action'].pause(0.5)
            for symbol in address['address']:
                browser['action'].send_keys(symbol)
                browser['action'].pause(randint(3, 7) / 10)
            browser['action'].pause(2)
            browser['action'].send_keys(Keys.DOWN)
            browser['action'].pause(1)
            browser['action'].send_keys(Keys.ENTER)
            browser['action'].pause(2)
            browser['action'].move_to_element(combobox_ok_button)
            browser['action'].click()
            browser['action'].pause(5)
            browser['action'].perform()

        for _key, _xpath in xpaths['delivery_address_buttons'].items():
            try:
                delivery_address_button = browser['client'].find_element(By.XPATH, _xpath)
                break
            except:
                continue
        else:
            with open('lavka.html', 'w') as f:
                f.write(browser['client'].page_source)
            raise Exception('Cant find!')

        if (
                any([word in delivery_address_button.text.lower() for word in ['укажите', 'адрес']])
                or
                clean_string(delivery_address_button.text).replace(' ,', ',').lower() not in address['address'].lower()
        ):
            browser['action'].move_to_element(delivery_address_button)
            browser['action'].click(delivery_address_button)
            browser['action'].perform()
            change_shop_address()

    try:
        ts = datetime.datetime.now()
        query_url = 'https://lavka.yandex.ru/213/search?text={value}'.format(**task)

        xpaths = {
            'delivery_address_buttons': {
                'delivery_address_button_without_address': './/header//button[contains(@class, "a71den4")]',
                'delivery_address_button_with_address': './/header//span[contains(@class, "cwhihqp")]',
            },
        }

        browser['client'].get(query_url)
        # sleep(2)
        status = check_status()
        if not status['status']:
            return status
        check_delivery_address()

        # Load more items
        for _ in range(3):
            browser['action'].send_keys(Keys.PAGE_DOWN)
            browser['action'].pause(1)
        browser['action'].send_keys(Keys.HOME)
        browser['action'].pause(1)
        browser['action'].perform()

        parsed_data = parse(html_data=browser['client'].page_source)
        print(f"[F:scrap][D:D] Parsed data len: {len(parsed_data)}; Data type: {type(parsed_data)}")
        parsed_ts = datetime.datetime.utcnow()

        results = []
        # Adding some data
        for product in parsed_data:
            brand, subbrand = '', ''
            for k_brand, v_subbrands in task["dictionary"].items():
                for k_subbrand, typings in v_subbrands.items():
                    for typing in typings:
                        if typing.lower() in product['title'].lower():
                            brand = k_brand
                            subbrand = k_subbrand
            product_data = {
                'result_id': task['id'],
                'ts': task['ts'],
                'date_parsed': parsed_ts.date(),
                'kw_or_url': task['value'],
                'shop_address': address['address'],
                'position': int(product['position']),
                'product_id': product['product_id'],
                'lavka_brand': '',
                'title': clean_string(product['title']),
                'link': product['link'],
                'image_link': product['image_link'],
                'price': float(product['price']),
                'oldprice': float(product['oldprice']) if product.get('oldprice') else 0.0,
                'client': task['client'],
                'category': task['category'],
                'subcategory': task['subcategory'],
                'brand': brand,
                'subbrand': subbrand,
                'device': 'D',
                'status': True,
                'stocks': 0
            }
            results.append(product_data)
        # wait(parsed_ts)
    except Exception as e:
        exc_ts = datetime.datetime.timestamp(datetime.datetime.now())
        browser['client'].save_screenshot(f"/code/shared/debug/screenshots/lavka_{exc_ts}.png")
        with open(f'/code/shared/debug/htmls/lavka_{exc_ts}.html', 'w') as f:
            f.write(browser['client'].page_source)
        print(f"[F:scrap,try/except][D:D][ts:{exc_ts}] {format_error(e=e)}")
        sleep(randint(2, 4))
        return {
            'status': False,
            'message': str(e)
        }

    print(f"[F:scrap][D:D] Return data len: {len(results)}")
    sleep(randint(2, 4))
    return {
            'status': True,
            'data': results
        }


def get_and_parse_json(data):

    browser_obj = check_and_get_browser_client(data['marketplace'])

    results = []
    for address in data["addresses"]:
        response = scrap(task=data, address=address, browser=browser_obj)
        if response['status']:
            results.extend(response['data'])
        elif not response['status'] and response['message'] == "500 ERROR":
            continue

    return results


if __name__ == '__main__':
    tasks = [
        {
            'id': 'bd9086c3-af99-45d6-a7d0-74e29e211504',
            'client': 'Pepsi',
            'category': 'Напитки',
            'subcategory': 'Энергетики',
            'type': 'search',
            'value': 'энергетик',
            'dictionary': {
                'PepsiCo': {
                    'Adrenaline Rush': [
                        'adrenaline', 'adrenaline rush', 'adrenaline', 'adrenaline game fuel', 'adrenalin zero'
                    ],
                    'Drive Me': [
                        'drive me', 'драйв ми'
                    ]
                },
                'Flash Up': {
                    'Flash Up': ['flash up']
                },
                'Red Bull': {'Red Bull': ['red bull']},
                'The Scandalist Energy Drink': {'The Scandalist Energy Drink': ['scandalist energy drink']},
                'Monster': {'Monster': ['monster', 'монстер']}, 'М-150': {'М-150': ['м-150']},
                'Таргет': {'Таргет': ['таргет']}, 'ALLIGATOR': {'ALLIGATOR': ['alligator', 'alligator']},
                'Fitness Food Factory': {'Fitness Food Factory WK UP': ['fitness food factory wk up']},
                'IMBA ENERGY': {'IMBA ENERGY': ['imba energy']}},
            'marketplace': 'lavka',
            'addresses': [
                {
                    'address': 'Россия, Москва, Ленинградский проспект, 15с1',
                    'lon': 37.57208167406319,
                    'lat': 55.781544555606494,
                }
            ],
            'ts': '2022-12-09 08:00:05'
        },
        {
            'id': 'bd9086c3-af99-45d6-a7d0-74e29e211504',
            'client': 'Pepsi',
            'category': 'Напитки',
            'subcategory': 'Энергетики',
            'type': 'search',
            'value': 'энергетик без сахара',
            'marketplace': 'lavka',
            'addresses': [
                {
                    'address': 'Россия, Москва, Ленинградский проспект, 15с1',
                    'lon': 37.57208167406319,
                    'lat': 55.781544555606494,
                }
            ],
            'dictionary': {
                'PepsiCo': {
                    'Adrenaline Rush': [
                        'adrenaline', 'adrenaline rush', 'adrenaline', 'adrenaline game fuel', 'adrenalin zero'
                    ],
                    'Drive Me': [
                        'drive me', 'драйв ми'
                    ]
                },
                'Flash Up': {
                    'Flash Up': ['flash up']
                },
                'Red Bull': {'Red Bull': ['red bull']},
                'The Scandalist Energy Drink': {'The Scandalist Energy Drink': ['scandalist energy drink']},
                'Monster': {'Monster': ['monster', 'монстер']}, 'М-150': {'М-150': ['м-150']},
                'Таргет': {'Таргет': ['таргет']}, 'ALLIGATOR': {'ALLIGATOR': ['alligator', 'alligator']},
                'Fitness Food Factory': {'Fitness Food Factory WK UP': ['fitness food factory wk up']},
                'IMBA ENERGY': {'IMBA ENERGY': ['imba energy']}},
        },
        {
            'id': 'bd9086c3-af99-45d6-a7d0-74e29e211504',
            'client': 'Pepsi',
            'category': 'Напитки',
            'subcategory': 'Энергетики',
            'type': 'search',
            'value': 'адреналин раш',
            'marketplace': 'lavka',
            'addresses': [
                {
                    'address': 'Россия, Москва, Ленинградский проспект, 15с1',
                    'lon': 37.57208167406319,
                    'lat': 55.781544555606494,
                }
            ],
            'dictionary': {
                'PepsiCo': {
                    'Adrenaline Rush': [
                        'adrenaline', 'adrenaline rush', 'adrenaline', 'adrenaline game fuel', 'adrenalin zero'
                    ],
                    'Drive Me': [
                        'drive me', 'драйв ми'
                    ]
                },
                'Flash Up': {
                    'Flash Up': ['flash up']
                },
                'Red Bull': {'Red Bull': ['red bull']},
                'The Scandalist Energy Drink': {'The Scandalist Energy Drink': ['scandalist energy drink']},
                'Monster': {'Monster': ['monster', 'монстер']}, 'М-150': {'М-150': ['м-150']},
                'Таргет': {'Таргет': ['таргет']}, 'ALLIGATOR': {'ALLIGATOR': ['alligator', 'alligator']},
                'Fitness Food Factory': {'Fitness Food Factory WK UP': ['fitness food factory wk up']},
                'IMBA ENERGY': {'IMBA ENERGY': ['imba energy']}},
        },
    ]
    for task in tasks:
        get_and_parse_json(data=task)
