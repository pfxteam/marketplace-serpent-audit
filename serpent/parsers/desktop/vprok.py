import datetime
import json
from random import randint
from time import sleep

from lxml import html
from lxml.html import HtmlElement, Element
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from serpent.parsers.exceptions import LoadErrorAfterBrowserCheck, AddressFormError, NoItems
from serpent.parsers.helpers import check_and_get_browser_client, lxml_get_inner_text, clean_string, format_error


def get_rating(element: Element) -> int:
    rating_element = element.xpath(
        './/div[contains(@class, "-product__rating")]'
        '//span[contains(@class, "-product-rating__visual")]'
    )
    if rating_element:
        class_name_text = rating_element[0].get('class')
        return int(class_name_text.split('product-rating__visual--')[-1])
    else:
        return 0


def parse_json(data: dict) -> list[dict]:
    result = []
    products = []
    if data.get('props'):
        if data['props'].get('pageProps'):
            if data['props']['pageProps'].get('initialStore'):
                if data['props']['pageProps']['initialStore'].get('searchPage'):
                    if data['props']['pageProps']['initialStore']['searchPage'].get('products'):
                        products = data['props']['pageProps']['initialStore']['searchPage']['products']
                    else:
                        print('[D:D][F:parse_json] No "products" key.')
                else:
                    print('[D:D][F:parse_json] No "searchPage" key.')
            else:
                print('[D:D][F:parse_json] No "initialStore" key.')
        else:
            print('[D:D][F:parse_json] No "pageProps" key.')
    else:
        print('[D:D][F:parse_json] No "props" key.')

    if not products:
        return []

    for pos, product in enumerate(products, start=1):

        image_url = product['images'][0]['url']
        if '<SIZE>' in image_url:
            image_url = image_url.replace('<SIZE>', 'x155')

        result.append({
            'position': pos,
            'product_id': product['productId'],
            'title': product['name'],
            'rating': product['rating'],
            'link': product['url'],
            'image_link': image_url,
            'price': product['price'],
            'oldprice': product['oldPrice']
        })

    return result


def parse_html(html_data: str):
    print("[D:D][F:parse] Starting work with HTML data.")

    ts = datetime.datetime.now()
    result = []

    xpaths = {
        'items': [
            './/ul[@id="catalogItems"]/li[contains(@class, "-catalog__item")]',
            './/ul[@id="catalogItems"]/li[contains(@class, "-catalog-product")]',
            './/div[contains(@class, "LayoutListing_body")]//article[contains(@class, "ListingProductTile")]',
        ],  # [Element]
        'image_link': [
            './/div[contains(@class, "-product__picture")]//img/@src',
            './/div[contains(@class, "image")]//img/@src',
            './/div[contains(@class, "Gallery")]//img/@src',
        ],  # str
        'product_id': [
            './/div[@data-id]/@data-id'
        ],  # str
        'link': [
            './/a[contains(@class, "-product__main-link")]/@href',
            './/a[contains(@class, "ListingProductTile_link")]/@href',
            './/a[contains(@class, "ile_link")]/@href',
        ],  # str
        'title': [  # Element
            './/a[contains(@class, "-product-title")]',
            './/a[contains(@class, "-product__title")]',
            './/div[contains(@class, "-product-title")]',
            './/div[contains(@class, "-product__title")]',
            './/p[contains(@class, "ListingProductTile_title")]',
            './/p[contains(@class, "ile_title")]',
            './/p[contains(@class, "title")]',
            './/a[contains(@class, "ile_link")]',

        ],
        'out': [
            './/div[contains(@class, "-product__out")]',
            './/div[contains(@class, "UnavailableInformer")]',
        ],  # bool
        'oldprice': [
            './/div[contains(@class, "-product-cost__old-price")]//div[@data-cost]/@data-cost',
            './/div[contains(@class, "OldPrice")]//div[contains(@class, "role_old")]',
            './/div[contains(@class, "OldPrice")]//div[contains(@class, "price")]',
        ],  # str | Element
        'price': [
            './/div[not(contains(@class, "old-cost")) and @data-cost]',
            './/div[contains(@class, "BuyBlock")]//div[contains(@class, "role_discount")]',
            './/div[contains(@class, "BuyBlock")]//div[contains(@class, "desktopFlex")]//div[contains(@class, "price")]',
        ],  # str

    }
    tree: HtmlElement = html.fromstring(
        html=html_data,
        parser=html.HTMLParser(encoding='utf-8')
    )
    for _xpath in xpaths['items']:
        items = tree.xpath(_xpath)
        if items:
            break
    else:
        raise NoItems("No items!")

    xpaths.pop('items')
    for position, item in enumerate(items, start=1):
        data = {
            'position': position,
            'status': True,
            'stocks': 0,
            'ts': ts,
            'rating': get_rating(item)
        }
        # Fill
        for field in xpaths.keys():
            if not data['status'] and field in ['price', 'oldprice']:
                data[field] = 0
                continue
            for _xpath in xpaths[field]:

                if field == 'out' and item.xpath(_xpath):
                    data[field] = False
                    break
                elif data['status'] is False and field in ['oldprice', 'price']:
                    data[field] = 0
                    break

                res = item.xpath(_xpath)
                if res:
                    if isinstance(res[0], str):
                        data[field] = str(res[0])
                    else:  # It's Element
                        data[field] = clean_string(lxml_get_inner_text(res[0]))
                    break
        # Fix
        for field in ['oldprice', 'price']:
            if data.get(field):
                data[field] = data[field].strip()
                if " " in data[field]:
                    data[field] = data[field].split(' ')[0].replace(',', '.')
            else:
                data[field] = 0

        if not data.get('product_id'):
            parts = data['link'].split('--')
            parts.reverse()
            for part in parts:
                try:
                    data['product_id'] = int(part)
                    break
                except:
                    continue
            else:
                data['product_id'] = 0

        result.append(data)

    return result


def scrap(task, browser, address):
    print(
        f"[D:D][F:scrap] Task data: {task} ({type(task)});\n"
        f"Browser: {browser};\n"
        f"Address data: {address} ({type(address)})"
    )

    xpaths = {

        # Catalog Items
        'catalog_items': [
            './/ul[@id="catalogItems"]/li[contains(@class, "-catalog__item")]',
            './/ul[@id="catalogItems"]/li[contains(@class, "-catalog-product")]',
            './/div[contains(@class, "LayoutListing_body")]//article[contains(@class, "ListingProductTile")]',
            './/div[contains(@class, "LayoutListing_body")]//article[contains(@class, "ProductTile")]',
        ],

        # Other Elements
        'delivery_address_button': [
            './/div[contains(@class, "SecondHeader_container")]/div[contains(@class, "LocationTile")]/span',
            './/div[contains(@class, "SecondHeader_container")]/div[contains(@class, "LocationTile")]',
            './/div[contains(@class, "header__address-filled")]/span',
            './/div[contains(@class, "header__address-filled")]',
            './/div[contains(@class, "header__address-empty")]',
        ],

        # Address Modal
        'modal_address_input': [
            './/div[contains(@class, "Modal_modal")]//input[@name="address"]',
            './/div[contains(@class, "fo-address-delivery__form")]//input[@name="address"]',
            './/input[@name="address"]',

        ],
        'modal_flat_input': [
            './/div[contains(@class, "Modal_modal")]//input[@name="flat"]',
            './/div[contains(@class, "address-delivery")]//input[@name="flat"]',
        ],
        'modal_submit_button': [
            './/div[contains(@class, "Modal_modal")]//button[@type="submit"]',
            './/button[contains(@class, "address-delivery__save")]',
        ],
        'modal_options': [
            './/ul[contains(@class, "Options_list")]//li[contains(@class, "Options_item")]',
            './/ul[contains(@class, "autocomplete")]//li[contains(@class, "autocomplete") and contains(@class, "item")]',
        ],

        # Age popup
        'confirm_age_popup': [
            './/div[contains(@class, "_confirm_dialog") and contains(@class, "_active")]',
        ],
        'confirm_age_yes_button': [
            './/div[contains(@class, "_confirm_dialog") and contains(@class, "_active")]//*[contains(@class, "confirm-popup-btn-ok")]',
        ]
    }

    def check_age_popup():
        for _xpath in xpaths['confirm_age_popup']:
            try:
                browser['client'].find_element(By.XPATH, _xpath)
                for _xpath in xpaths['confirm_age_yes_button']:
                    try:
                        age_check_yes_button = browser['client'].find_element(By.XPATH, _xpath)
                        browser['action'].move_to_element(age_check_yes_button)
                        browser['action'].click()
                        browser['action'].perform()
                        return {'status': True}
                    except:
                        pass
            except:
                pass
        return {'status': False}


    def check_delivery_address():

        def change_shop_address():

            for _xpath in xpaths['modal_address_input']:
                try:
                    modal_address_input = browser['client'].find_element(
                        by=By.XPATH,
                        value=_xpath
                    )
                    break
                except:
                    continue
            else:
                raise AddressFormError("Can't find address input in modal window.")

            for _xpath in xpaths['modal_flat_input']:
                try:
                    modal_flat_input = browser['client'].find_element(
                        by=By.XPATH,
                        value=_xpath
                    )
                    break
                except:
                    continue
            else:
                raise AddressFormError("Can't find flat input in modal window.")

            for _xpath in xpaths['modal_submit_button']:
                try:
                    modal_submit_button = browser['client'].find_element(
                        by=By.XPATH,
                        value=_xpath
                    )
                    break
                except:
                    continue
            else:
                raise AddressFormError("Can't find submit button in modal window.")

            browser['action'].move_to_element(modal_address_input)
            browser['action'].click(modal_address_input)
            browser['action'].pause(1)
            # browser['action'].perform()
            # while len(modal_address_input.get_attribute('value')):
            #     modal_address_input.send_keys(Keys.BACKSPACE)
            #     modal_address_input.send_keys(Keys.DELETE)
            for _ in range(3):
                browser['action'].move_to_element(modal_address_input).click()
            browser['action'].pause(0.5)
            for symbol in address['address']:
                browser['action'].send_keys(symbol)
                browser['action'].pause(randint(3, 7) / 10)
            browser['action'].pause(2)
            browser['action'].perform()
            for _xpath in xpaths['modal_options']:
                try:
                    options = browser['client'].find_elements(
                        by=By.XPATH,
                        value=_xpath
                    )
                    if not options:
                        continue
                    browser['action'].move_to_element(options[0])
                    browser['action'].click()
                    browser['action'].pause(1)
                    browser['action'].move_to_element(modal_flat_input)
                    browser['action'].click()
                    browser['action'].pause(1)
                    browser['action'].send_keys(1)
                    browser['action'].pause(1)
                    browser['action'].move_to_element(modal_submit_button)
                    browser['action'].click()
                    browser['action'].pause(2)
                    browser['action'].perform()
                    break
                except:
                    continue
            else:
                raise AddressFormError("Can't find options in modal window.")

        for _xpath in xpaths['delivery_address_button']:
            try:
                delivery_address_button = browser['client'].find_element(By.XPATH, _xpath)
                if not delivery_address_button.text:
                    continue
                break
            except:
                continue
        else:
            raise Exception('Cant find!')

        if (
                any([word in delivery_address_button.text.lower() for word in ['укажите', 'адрес']])
                or
                clean_string(delivery_address_button.text).lower() not in address['address'].lower()
        ):
            browser['action'].move_to_element(delivery_address_button)
            browser['action'].click(delivery_address_button)
            browser['action'].pause(2)
            browser['action'].perform()
            change_shop_address()

    def load_page(url: str):
        browser['client'].get(url)
        sleep(1)

        # Wait for full page load
        if all([word in browser['client'].page_source.lower() for word in ['выполняется', 'проверка', 'браузер']]):
            while all(
                    [word in browser['client'].page_source.lower() for word in ['выполняется', 'проверка', 'браузер']]):
                sleep(0.2)
        retries, max_retry = 0, 20  # 20 times check
        while retries < max_retry:
            for _xpath in xpaths['catalog_items']:
                try:
                    items = browser['client'].find_element(By.XPATH, _xpath)
                    if items:
                        return
                except:
                    continue
            else:
                retries += 1
                sleep(1)

        else:
            logger.info(f"[F:load_page] Check HTML code, can't find items.")
            raise LoadErrorAfterBrowserCheck("Check HTML code, can't find items.")


    try:
        query_url = 'https://www.vprok.ru/catalog/search?text={value}&sort=relevance_desc'.format(**task)

        load_page(url=query_url)
        check_age_popup()
        check_delivery_address()

        parsed_data = []
        try:
            props_data = browser['client'].find_element(By.ID, '__NEXT_DATA__').get_attribute('innerHTML')
            props_data = json.loads(props_data)
            parsed_data = parse_json(data=props_data)
        except:
            pass
        if not parsed_data:
            # Load more items
            for _ in range(3):
                browser['action'].send_keys(Keys.PAGE_DOWN)
                browser['action'].pause(1)
            browser['action'].send_keys(Keys.HOME)
            browser['action'].pause(2)
            browser['action'].perform()
            parsed_data = parse_html(html_data=browser['client'].page_source)
        print(
            f"[D:D][F:scrap] Parsed data len: {len(parsed_data)}; Data type: {type(parsed_data)}"
        )
        parsed_ts = datetime.datetime.utcnow()

        parse_results = []
        # Adding some data
        for product in parsed_data:
            brand, subbrand = '', ''
            for k_brand, v_subbrands in task["dictionary"].items():
                for k_subbrand, typings in v_subbrands.items():
                    for typing in typings:
                        if typing.lower() in product['title'].lower():
                            brand = k_brand
                            subbrand = k_subbrand
            product_data = {
                'result_id': task['id'],
                'ts': task["ts"],
                'date_parsed': parsed_ts.date(),
                'kw_or_url': task['value'],
                'position': int(product['position']),
                'product_id': int(product['product_id']),
                'title': product['title'],
                'sales_points': [],
                'rating': float(product['rating']),
                'link': product['link'],
                'image_link': product['image_link'],
                'price': float(product['price']),
                'oldprice': float(product['oldprice']) if product.get('oldprice') else 0.0,
                'client': task['client'],
                'category': task['category'],
                'subcategory': task['subcategory'],
                'brand': brand,
                'subbrand': subbrand,
                'device': 'D',
            }
            parse_results.append(product_data)
    except Exception as e:
        exc_ts = datetime.datetime.timestamp(datetime.datetime.now())
        browser['client'].save_screenshot(f"/code/shared/debug/screenshots/vprok_{exc_ts}.png")
        print(f"[D:D][F:scrap,try/except][ts:{exc_ts}] {format_error(e=e)}")
        if any([isinstance(e, exc) for exc in [NoItems, AddressFormError, LoadErrorAfterBrowserCheck]]):
            with open(f'/code/shared/debug/htmls/vprok_{exc_ts}.html', 'w') as f:
                f.write(browser['client'].page_source)
            print(
                f"[D:D][F:scrap,try/except][ts:{exc_ts}] {e.__class__.__name__}"
                f" exception met, stored HTML file to analyze situation."
            )
        sleep(randint(2, 4))
        return {
            'status': False,
            'message': str(e)
        }

    print(f"[D:D][F:scrap] Return data len: {len(parse_results)}")
    sleep(randint(2, 4))
    return {
        'status': True,
        'data': parse_results
    }


def get_and_parse_json(data):

    browser_obj = check_and_get_browser_client(data['marketplace'])

    results = []
    for address in data["addresses"]:
        response = scrap(task=data, address=address, browser=browser_obj)
        if response['status']:
            results.extend(response['data'])
        elif not response['status'] and response['message'] == "500 ERROR":
            continue
    return results


if __name__ == '__main__':
    tasks = [
        {
            'id': 'bd9086c3-af99-45d6-a7d0-74e29e211504',
            'client': 'Pepsi',
            'category': 'Напитки',
            'subcategory': 'Энергетики',
            'type': 'search',
            'value': 'энергетик',
            'marketplace': 'vprok',
            'addresses': [
                {
                    'address': 'Россия, Москва, Ленинградский проспект, 15с1',
                    'lon': 37.57208167406319,
                    'lat': 55.781544555606494,
                }
            ],
            'dictionary': {
                'PepsiCo': {
                    'Adrenaline Rush': [
                        'adrenaline', 'adrenaline rush', 'adrenaline', 'adrenaline game fuel', 'adrenalin zero'
                    ],
                    'Drive Me': [
                        'drive me', 'драйв ми'
                    ]
                },
                'Flash Up': {
                    'Flash Up': ['flash up']
                },
                'Red Bull': {'Red Bull': ['red bull']},
                'The Scandalist Energy Drink': {'The Scandalist Energy Drink': ['scandalist energy drink']},
                'Monster': {'Monster': ['monster', 'монстер']}, 'М-150': {'М-150': ['м-150']},
                'Таргет': {'Таргет': ['таргет']}, 'ALLIGATOR': {'ALLIGATOR': ['alligator', 'alligator']},
                'Fitness Food Factory': {'Fitness Food Factory WK UP': ['fitness food factory wk up']},
                'IMBA ENERGY': {'IMBA ENERGY': ['imba energy']}},
            'ts': '2022-12-09 08:00:05'
        },
        {
            'id': 'bd9086c3-af99-45d6-a7d0-74e29e211504',
            'client': 'Pepsi',
            'category': 'Напитки',
            'subcategory': 'Энергетики',
            'type': 'search',
            'value': 'пиво',
            'marketplace': 'vprok',
            'addresses': [
                {
                    'address': 'Россия, Москва, Ленинградский проспект, 15с1',
                    'lon': 37.57208167406319,
                    'lat': 55.781544555606494,
                }
            ],
            'dictionary': {},
        },
        {
            'id': 'bd9086c3-af99-45d6-a7d0-74e29e211504',
            'client': 'Pepsi',
            'category': 'Напитки',
            'subcategory': 'Энергетики',
            'type': 'search',
            'value': 'паста',
            'marketplace': 'vprok',
            'addresses': [
                {
                    'address': 'Россия, Москва, Ленинградский проспект, 15с1',
                    'lon': 37.57208167406319,
                    'lat': 55.781544555606494,
                }
            ],
            'dictionary': {},
        },
    ]
    for task in tasks:
        get_and_parse_json(data=task)
