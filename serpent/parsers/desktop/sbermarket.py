from datetime import datetime
from random import randint
from time import sleep

from lxml import html
from lxml.html import HtmlElement
from requests import Session
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from serpent.parsers.exceptions import NoItems, ScrapError, LoadErrorAfterBrowserCheck
from serpent.parsers.helpers import check_and_get_browser_client, lxml_get_inner_text, clean_string, format_error


def parse_html(html_data: str):

    parse_result = []

    xpaths = {
        'items': [
            './/ul[contains(@class, "MultiSearchProductsGrid_grid")]//li//div[contains(@class, "ProductCard_styles_root")]',
            './/ul[contains(@class, "MultiSearchProductsGrid_grid")]//li//div[contains(@class, "ProductCard_root")]',
            './/div[contains(@class, "ProductsGrid_styles_grid")]/div[contains(@class, "ProductCard_styles")]',
            './/div[contains(@class, "ProductsGrid_grid")]/div[contains(@class, "ProductCard")]',
        ],  # [Element]
        'image_link': ['.//img/@src'],  # str
        # 'product_id': ['./div/@data-item-id'],  # str
        'link': ['.//a[contains(@class, "ProductCardLink")]/@href'],  # str
        'title': [  # Element
            './/h3[contains(@class, "ProductCard_styles_title")]/@title',
            './/h3[contains(@class, "ProductCard_title")]/@title',
            './/h3[contains(@class, "ProductCard_styles_title")]/@aria-label',
            './/h3/@aria-label',
            './/h3[contains(@class, "ProductCard_styles_title")]',
            './/h3',
        ],
        'oldprice': [  # Element
            './/div[contains(@class, "ProductCardPrice_styles_originalPrice")]',
            './/div[contains(@class, "ProductCardPrice_originalPrice")]',
        ],
        'price': [  # Element
            './/div[contains(@class, "ProductCardPrice_styles_price")]',
            './/div[contains(@class, "ProductCardPrice_price")]',
        ],

    }
    tree: HtmlElement = html.fromstring(
        html=html_data,
        parser=html.HTMLParser(encoding='utf-8')
    )
    for _xpath in xpaths['items']:
        items = tree.xpath(_xpath)
        if items:
            break
    else:
        print(f"No items, check HTML code.\n{html_data}")
        raise NoItems("No items, check HTML code.")

    xpaths.pop('items')
    for position, item in enumerate(items, start=1):
        data = {
            'position': position,
            'status': True,
            'stocks': 0,
            'product_id': 0
        }
        # Fill
        for field in xpaths.keys():
            for _xpath in xpaths[field]:
                res = item.xpath(_xpath)
                if res:
                    if isinstance(res[0], str):
                        data[field] = str(res[0])
                    else:  # It's Element
                        data[field] = clean_string(lxml_get_inner_text(res[0]))
                    break
            if not data.get(field) and field not in ['oldprice']:
                raise Exception(f'No value for {field} field.')
        # Fix
        for field in ['oldprice', 'price']:
            if data.get(field):
                _new_value = data[field]
                _new_value = _new_value.split('шт.')[-1].strip()
                for currency in [
                    '₽.', '₽', 'руб.', 'р.', 'рублей', 'рубля'
                ]:
                    if currency in _new_value:
                        _new_value = _new_value[:_new_value.rfind(currency)].strip()
                        break
                data[field] = float(_new_value.replace(' ', '').replace(',', '.'))
            else:
                data[field] = 0.0

        parse_result.append(data)

    return parse_result


def scrap(task, address, browser):
    """
    Scrap task
    :param task: task dict with category, subcategory, type, keyword (value), dictionary, etc...
    :param address: address dict with shop ids, address name and position
    :param browser: browser dict with browser client and action chain
    :return:
    """
    print(
        f"[F:scrap][D:D] Task data: {task} ({type(task)});\n"
        f"Browser: {browser};\n"
        f"Address data: {address} ({type(address)})"
    )

    xpaths = {
        'delivery_map_button': [
            './/div[contains(@class, "MainBanner")]//button[contains(@class, "AddressWithMap")]'
        ],
        'delivery_map_address_input_on_modal': [
            './/div[contains(@class, "SearchSelectForMap")]/input',
            './/input[contains(@class, "SearchSelectForMap_input")]',
            './/input[contains(@class, "DeliveryMap_input")]',
            './/input[contains(@class, "AddressWithMap_styles_input")]',
        ],
        'delivery_map_remove_address_icon_on_modal': [
            './/div[contains(@class, "SearchSelectForMap_selectContainer")]//i[contains(@class, "styles_close")]',
        ],
        'delivery_map_find_shops_button_on_modal': [
            './/button[contains(@class, "DeliveryMap_button")]'
        ],
        'delivery_map_dropdown_options_on_modal': [
            './/input[contains(@class, "DeliveryMap_dropdown")]//div'
        ],

        # With address
        'delivery_address_button': [
            './/button[contains(@class, "AddressInput_root")]',
            './/div[contains(@class, "styles_addressContainer")]//span[@data-qa="current-ship-address"]',
            './/div[contains(@data-qa, "ship-address-selector")]//button[contains(@data-qa, "select")]',
        ],

        # Age check
        'age_check_checkbox_input': [
            './/input[@data-qa="disclaimer_modal_checkbox"]',
        ],
        'age_check_submit_button': [
            './/button[@data-qa="disclaimer_modal_ok_button"]',
        ]
    }

    def load_main_page():
        browser['client'].get('https://sbermarket.ru')

        # Wait for full page load
        if all([word in browser['client'].page_source.lower() for word in ['выполняется', 'проверка', 'браузер']]):
            while all(
                    [word in browser['client'].page_source.lower() for word in ['выполняется', 'проверка', 'браузер']]):
                sleep(0.2)
        loaded_footer = False
        retries, max_retry = 0, 30
        while not loaded_footer and retries < max_retry:
            for _xpath in [
                './/div[contains(@class, "-footer__container")]',
                './/div[contains(@class, "ooter_row")]',
                './/footer[contains(@class, "footer")]',
                './/div[contains(@class, "ooter_grid__")]',
            ]:
                try:
                    browser['client'].find_element(By.XPATH, _xpath)
                    loaded_footer = True
                    break
                except:
                    pass
            else:
                retries += 1
                sleep(1)

        if not loaded_footer:
            raise Exception('Check HTML code, cant find footer element.')

    def check_delivery_address():

        def change_shop_address():
            print("Starting change address..")
            for _xpath in xpaths['delivery_map_address_input_on_modal']:
                try:
                    delivery_map_address_input_on_modal = browser['client'].find_element(By.XPATH, _xpath)
                    break
                except:
                    continue
            else:
                raise Exception("Can't find address input.")

            # for _xpath in xpaths['delivery_map_remove_address_icon_on_modal']:
            #     try:
            #         delivery_map_remove_address_icon_on_modal = browser['client'].find_element(By.XPATH, _xpath)
            #         break
            #     except:
            #         continue
            # else:
            #     print(f"Can't find remove address icon.")
            #     # raise Exception("Can't find address input.")

            for _xpath in xpaths['delivery_map_find_shops_button_on_modal']:
                try:
                    delivery_map_find_shops_button_on_modal = browser['client'].find_element(By.XPATH, _xpath)
                    break
                except:
                    continue
            else:
                raise Exception("Can't find find shops button on modal window.")

            browser['action'].move_to_element(delivery_map_address_input_on_modal)
            browser['action'].click()
            browser['action'].pause(0.5)
            browser['action'].click()
            browser['action'].pause(0.5)
            while len(delivery_map_address_input_on_modal.get_attribute('value')):
                browser['action'].send_keys(Keys.BACKSPACE)
                browser['action'].send_keys(Keys.DELETE)
            for symbol in address['address']:
                browser['action'].send_keys(symbol)
                browser['action'].pause(randint(3, 7) / 10)
            browser['action'].pause(2)
            browser['action'].send_keys(Keys.DOWN)
            browser['action'].pause(0.5)
            browser['action'].send_keys(Keys.ENTER)
            browser['action'].pause(2)
            browser['action'].move_to_element(delivery_map_find_shops_button_on_modal)
            browser['action'].click()
            browser['action'].pause(5)
            browser['action'].perform()

        delivery_button = None
        for _xpath in xpaths['delivery_map_button']:
            try:
                delivery_button = browser['client'].find_element(By.XPATH, _xpath)
                print(f"Found delivery button without address by xpath = {_xpath}")
                browser['action'].move_to_element(delivery_button)
                browser['action'].click(delivery_button)
                browser['action'].pause(2)
                browser['action'].perform()
                print("Opened delivery map.")
                change_shop_address()
                return
            except:
                continue
        for _xpath in xpaths['delivery_address_button']:
            try:
                delivery_button = browser['client'].find_element(By.XPATH, _xpath)
                print(f"Found delivery button with address by xpath = {_xpath}")
                for address_part in address['address_parts']:
                    if (
                        address_part in delivery_button.get_attribute('title')
                        or address_part in delivery_button.text
                    ):
                        return
                else:
                    browser['action'].move_to_element(delivery_button)
                    browser['action'].click(delivery_button)
                    browser['action'].pause(2)
                    browser['action'].perform()
                    print("Opened delivery map.")
                    change_shop_address()
                    return
            except:
                continue
        if delivery_button is None:
            raise ScrapError('Problem with address button.')

    def get_products_via_api(store_id: int, keyword: str):
        browser_cookies = browser['client'].get_cookies()
        session = Session()
        for cookie in browser_cookies:
            session.cookies.set(cookie['name'], cookie['value'])
        response = session.get(
            url=f"https://sbermarket.ru/api/stores/{store_id}/products?q={keyword}&page=1&per_page=20&filter[root_category][]=all",
            headers={
                'accept': 'application/json, text/plain, */*',
                'accept-encoding': 'gzip, deflate, br',
                'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
                'api-version': '3.0',
                'is-storefront-ssr': 'false',
                'sec-ch-ua': '"Not?A_Brand";v="8", "Chromium";v="108", "Google Chrome";v="108"',
                'sec-ch-ua-mobile': '?0',
                'sec-ch-ua-platform': 'macOS',
                'sec-getch-dest': 'empty',
                'sec-fetch-mode': 'cors',
                'sec-feftch-site': 'same-origin',
                'client-token': '7ba97b6f4049436dab90c789f946ee2f',
                'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
            }
        )
        if response.status_code != 200:
            return {
                'result': False,
                'data': {},
            }
        return {
            'result': True,
            'data': response.json(),
        }

    def age_check():
        age_check_checkbox_input, age_check_submit_button = None, None
        for _xpath in xpaths['age_check_checkbox_input']:
            try:
                age_check_checkbox_input = browser['client'].find_element(By.XPATH, _xpath)
                break
            except:
                continue
        for _xpath in xpaths['age_check_submit_button']:
            try:
                age_check_submit_button = browser['client'].find_element(By.XPATH, _xpath)
                break
            except:
                continue
        if (
            '18 лет' in browser['client'].page_source
            or (age_check_checkbox_input and age_check_submit_button)
        ):
            browser['action'].move_to_element(age_check_checkbox_input)
            browser['action'].pause(1)
            browser['action'].click()
            browser['action'].pause(1)
            browser['action'].move_to_element(age_check_submit_button)
            browser['action'].pause(1)
            browser['action'].click()
            browser['action'].pause(5)
            browser['action'].perform()

    if "search?keywords" not in browser['client'].current_url:
        load_main_page()
    check_delivery_address()

    parse_results = []

    try:
        for shop in address['shops']:
            query_url = "https://sbermarket.ru/multisearch?q={kw}&sid={store_id}".format(
                kw=task['value'], store_id=shop['id']
            )
            browser['client'].get(query_url)
            age_check()

            # Load more items
            browser['action'].pause(1)
            for _ in range(3):
                browser['action'].send_keys(Keys.PAGE_DOWN)
                browser['action'].pause(1)
            browser['action'].send_keys(Keys.HOME)
            browser['action'].pause(1)
            browser['action'].perform()

            products = parse_html(browser['client'].page_source)

            parsed_ts = datetime.utcnow()
            for position, product in enumerate(products, start=1):
                brand, subbrand = '', ''
                for k_brand, v_subbrands in task["dictionary"].items():
                    for k_subbrand, typings in v_subbrands.items():
                        for typing in typings:
                            if typing.lower() in product['title'].lower():
                                brand = k_brand
                                subbrand = k_subbrand
                        if brand and subbrand:
                            break
                    if brand and subbrand:
                        break
                product_data = {
                    'result_id': task['id'],
                    'ts': task["ts"],
                    'date_parsed': parsed_ts.date(),
                    'kw_or_url': task['value'],
                    'shop_name': shop['name'],
                    'shop_address': address['address'],
                    'position': int(position),
                    'product_id': int(product.get('id', product.get('product_id', 0))),
                    'product_sku': int(product.get('sku', 0)),
                    'product_retailer_sku': int(product.get('retailer_sku', 0)),
                    'title': product['title'],
                    'rating': float(product.get('score', 0)),
                    'review_count': int(
                        product['score_details']['comment_count']
                        if product.get('score_details') and product['score_details'].get('comment_count')
                        else 0
                    ),
                    'link': product.get('canonical_url', product.get('link', '')),
                    'image_link': (
                        product['image_urls'][0]
                        if product.get('image_urls')
                        else product.get('image_link', '')
                    ),
                    'sales_points': (
                        [str(b) for b in product['promo_badge_ids']]
                        if product.get('promo_badge_ids')
                        else []
                    ),
                    'price': float(product['price']),
                    'oldprice': float(product.get('original_price', product.get('oldprice', 0))),
                    'client': task['client'],
                    'category': task['category'],
                    'subcategory': task['subcategory'],
                    'brand': brand,
                    'subbrand': subbrand,
                    'device': 'D'
                }
                parse_results.append(product_data)
    except Exception as e:
        exc_ts = datetime.timestamp(datetime.now())
        browser['client'].save_screenshot(f"/code/shared/debug/screenshots/sbermarket_{exc_ts}.png")
        print(f"[F:scrap,try/except][ts:{exc_ts}] {format_error(e=e)}")
        if any([isinstance(e, exc) for exc in [NoItems, LoadErrorAfterBrowserCheck]]):
            with open(f'/code/shared/debug/htmls/sbermarket_{exc_ts}.html', 'w') as f:
                f.write(browser['client'].page_source)
            print(
                f"[D:D][F:scrap,try/except][ts:{exc_ts}] {e.__class__.__name__}"
                f" exception met, stored HTML file to analyze situation."
            )
        sleep(randint(2, 4))
        return {
            'status': False,
            'message': str(e)
        }

    print(f"[F:scrap][D:D] Return data len: {len(parse_results)}")
    sleep(randint(2, 4))
    return {
        'status': True,
        'data': parse_results
    }


def get_and_parse_json(data):

    browser_obj = check_and_get_browser_client(data['marketplace'])

    results = []
    for address in data["addresses"]:
        parsed_data = scrap(task=data, address=address, browser=browser_obj)
        if parsed_data['status']:
            results.extend(parsed_data['data'])
    return results


if __name__ == '__main__':
    tasks = [
        {
            'id': 'bd9086c3-af99-45d6-a7d0-74e29e211504',
            'client': 'Pepsi',
            'category': 'Напитки',
            'subcategory': 'Энергетики',
            'type': 'search',
            'value': 'энергетик',
            'dictionary': {
                'PepsiCo': {
                    'Adrenaline Rush': [
                        'adrenaline', 'adrenaline rush', 'adrenaline', 'adrenaline game fuel', 'adrenalin zero'
                    ],
                    'Drive Me': [
                        'drive me', 'драйв ми'
                    ]
                },
                'Flash Up': {
                    'Flash Up': ['flash up']
                },
                'Red Bull': {'Red Bull': ['red bull']},
                'The Scandalist Energy Drink': {'The Scandalist Energy Drink': ['scandalist energy drink']},
                'Monster': {'Monster': ['monster', 'монстер']}, 'М-150': {'М-150': ['м-150']},
                'Таргет': {'Таргет': ['таргет']}, 'ALLIGATOR': {'ALLIGATOR': ['alligator', 'alligator']},
                'Fitness Food Factory': {'Fitness Food Factory WK UP': ['fitness food factory wk up']},
                'IMBA ENERGY': {'IMBA ENERGY': ['imba energy']}},
            'ts': '2022-12-09 08:00:05'
        },
        {
            'id': 'bd9086c3-af99-45d6-a7d0-74e29e211504',
            'client': 'Pepsi',
            'category': 'Напитки',
            'subcategory': 'Энергетики',
            'type': 'search',
            'value': 'пиво',
            'dictionary': {},
        },
        {
            'id': 'bd9086c3-af99-45d6-a7d0-74e29e211504',
            'client': 'Pepsi',
            'category': 'Напитки',
            'subcategory': 'Энергетики',
            'type': 'search',
            'value': 'паста',
            'dictionary': {},
        },
    ]
    for task in tasks:
        get_and_parse_json(data=task)
