import datetime
import json
import random
import shlex
import subprocess
from datetime import datetime, timezone
from os import getenv
from pprint import pprint
from urllib.parse import quote

from serpent.parsers.exceptions import GetAPIError
from serpent.parsers.helpers import parse_int, parse_float, clean_html


def get_html(data):
    print(f"[F:get_html] Got data: {data}")
    ts = datetime.now(timezone.utc)
    headers = {
        "User-Agent": f"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/10{random.randint(1, 4)}.0.0.0 Safari/537.36",
        "Host": "api.ozon.ru"
    }
    proxies = [
        {
            'https': f'http://{getenv("PROXY_OZON_USER")}:{getenv("PROXY_OZON_PSWD")}@{getenv("PROXY_OZON_HOST")}:{getenv("PROXY_OZON_PORT")}',
            'http': f'http://{getenv("PROXY_OZON_USER")}:{getenv("PROXY_OZON_PSWD")}@{getenv("PROXY_OZON_HOST")}:{getenv("PROXY_OZON_PORT")}',
        }
    ]
    # subprocess curl
    if '/category/' not in data['value']:
        cmd = f'''
        curl -i --location --proxy {proxies[0]['http']} --request GET 'https://api.ozon.ru/composer-api.bx/page/json/v2?url=/search/?from_global=true&text={quote(data["value"])}' \
        --header 'Host: {headers['Host']}' \
        --header 'User-Agent: {headers['User-Agent']}'
        '''
    else:
        cmd = f'''
        curl -i --location --proxy {proxies[0]['http']} --request GET 'https://api.ozon.ru/composer-api.bx/page/json/v2?url={data["value"]}' \
        --header 'Host: {headers['Host']}' \
        --header 'User-Agent: {headers['User-Agent']}'
        '''
    print(f"Command: {cmd}")
    args = shlex.split(cmd)
    process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    tmp = stdout.decode('utf-8').split('ma=86400\r\n\r\n{')
    stdout = '{' + tmp[1]
    if 'HTTP/2 200' not in tmp[0] and 'HTTP/2 302' not in tmp[0]:
        raise GetAPIError(f'Response status code is bad :(')
    res = json.loads(stdout)

    modal = [v for k, v in res['widgetStates'].items() if 'userAdultModal-' in k]
    if modal and 'searchResultsV2-' not in res['widgetStates'].items():
        print('ADULT BAD MODAL!!!')

        cmd = '''
            curl -c cookies.txt -X POST -H 'Accept: */*' -H 'Accept-Encoding: gzip, deflate' -H 'Connection: keep-alive' -H 'Content-Length: 199' -H 'Content-Type: application/json' -H 'User-Agent: python-requests/2.28.1' -d '{"link": "https://www.ozon.ru/search/?text=%D1%8D%D0%BD%D0%B5%D1%80%D0%B3%D0%B5%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9+%D0%BD%D0%B0%D0%BF%D0%B8%D1%82%D0%BE%D0%BA", "birthdate": "1986-06-21"}' 'https://api.ozon.ru/composer-api.bx/_action/setBirthdate?isAlco=false'
            '''
        args = shlex.split(cmd)
        process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()

        if '/category/' not in data['value']:
            cmd = f'''
            curl -b cookies.txt -i --location --proxy {proxies[0]['http']} --request GET 'https://api.ozon.ru/composer-api.bx/page/json/v2?url=/search/?from_global=true&text={quote(data["value"])}' \
            --header 'Host: {headers['Host']}' \
            --header 'User-Agent: {headers['User-Agent']}'
            '''
        else:
            cmd = f'''
            curl -b cookies.txt -i --location --proxy {proxies[0]['http']} --request GET 'https://api.ozon.ru/composer-api.bx/page/json/v2?url={data["value"]}' \
            --header 'Host: {headers['Host']}' \
            --header 'User-Agent: {headers['User-Agent']}'
            '''
        args = shlex.split(cmd)
        process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        tmp = stdout.decode('utf-8').split('ma=86400\r\n\r\n{')
        stdout = '{' + tmp[1]
        if 'HTTP/2 200' not in tmp[0] and 'HTTP/2 302' not in tmp[0]:
            raise GetAPIError(f'Response status code is bad :(')
        res = json.loads(stdout)

    items = [v for k, v in res['widgetStates'].items() if 'searchResultsV2-' in k][0]
    if not items:
        print('NO ITEMS!!!')
        raise GetAPIError(f'Response is: {res}')
    items = json.loads(items)['items']
    if items is None:
        print('NO ITEMS!!!')
        raise GetAPIError(f'Response is: {res}')
    brandshelf_data = [v for k, v in res['widgetStates'].items() if 'skuBrandShelf-' in k]
    brandshelf_data = brandshelf_data[0] if brandshelf_data else False
    city = res['location']['current']['city']
    tracking_payloads = res['trackingPayloads']
    data['items'] = items
    data['bs'] = brandshelf_data
    data['city'] = city
    data['trackingPayloads'] = tracking_payloads
    data['ts'] = ts
    print(data['dictionary'])

    return data


def parse_searchresults(data):
    address = data['city']
    tracking_payloads = data['trackingPayloads']
    print(f"GOT HTML FOR REGION: {address}, {len(data['items'])}")
    results = data['items']
    final_results = list()
    parsed_ts = datetime.utcnow()
    for idx, result in enumerate(results):
        (
            price,
            oldprice,
            title,
            seller,
            delivery,
            sales_points,
            rating,
            rating_count,
            brand,
            subbrand,
            ozon_brand,
            ozon_brandid,
            ozon_seller_id,
            ozon_stock_count,
        ) = (
            0,
            0,
            "False",
            "False",
            "False",
            [],
            0,
            0,
            "False",
            "False",
            "False",
            0,
            0,
            0,
        )
        for state in result["mainState"]:
            if state["atom"]["type"] == "price":
                if 'price' not in state["atom"]["price"]:
                    print(result["mainState"])
                price = int(
                    state["atom"]["price"]["price"]
                    .replace(" ", "")
                    .replace("₽", "") if 'price' in state["atom"]["price"] else state["atom"]["price"]["originalPrice"]
                    .replace(" ", "")
                    .replace("₽", "")
                )
                if "originalPrice" in state["atom"]["price"]:
                    oldprice = int(
                        state["atom"]["price"]["originalPrice"].replace(" ", "").replace("₽", "")
                    )
            if state["atom"]["type"] == "gradientBadge":
                price = int(
                    state["atom"]["gradientBadge"]["primaryText"].replace(" ", "").replace("₽", "")
                )
            elif state["atom"]["type"] == "textAtom" and 'id' in state and state["id"] == "name":
                title = state["atom"]["textAtom"]["text"]
            elif state["atom"]["type"] == "labelList":
                for label in state["atom"]["labelList"]["items"]:
                    if 'icon' in label and label["icon"]["image"] == 'ic_s_star_filled':
                        rating = parse_float(label['title'])
                    elif 'icon' in label and label["icon"]["image"] == 'ic_s_dialog_compact':
                        rating_count = parse_int(label['title'])
                    else:
                        sales_points.append(label['title'])
            else:
                pass

        if 'rightState' in result:
            for state in result["rightState"]:
                if state["atom"]["type"] == "price":
                    price = int(
                        state["atom"]["price"]["price"]
                        .replace(" ", "")
                        .replace("₽", "") if 'price' in state["atom"]["price"] else state["atom"]["price"]["originalPrice"]
                        .replace(" ", "")
                        .replace("₽", "")
                    )
                    if "originalPrice" in state["atom"]["price"]:
                        oldprice = int(
                            state["atom"]["price"]["originalPrice"].replace(" ", "").replace("₽", "")
                        )

        delivery_text = ''
        seller = "False"
        delivery = "False"
        if "multiButton" in result and "ozonSubtitle" in result["multiButton"]:
            delivery_text = clean_html(
                result["multiButton"]["ozonSubtitle"]["textAtomWithIcon"]["text"]
            )
            try:
                delivery, seller = [t.strip() for t in delivery_text.split(',')]
            except Exception as e:
                print(e)

        if "multiButton" in result and "expressButton" in result["multiButton"]:
            delivery_text = clean_html(
                result["multiButton"]["expressSubtitle"]["textAtomWithIcon"]["text"]
            )
            try:
                delivery, seller = [t.strip() for t in delivery_text.split(',')]
            except Exception as e:
                print(e)


        tracking = result["trackingInfo"]["click"]["key"]
        if tracking in tracking_payloads:
            print(pprint(tracking_payloads[tracking]))
            tracking_payload = json.loads(tracking_payloads[tracking])
            ozon_brand = tracking_payload["brandName"] if 'brandName' in tracking_payload else 'Undefined'
            ozon_brandid = int(tracking_payload["brandId"] if 'brandId' in tracking_payload else 0)
            ozon_seller_id = int(tracking_payload["sellerId"] if 'sellerId' in tracking_payload else 0)
            ozon_stock_count = int(tracking_payload["stockCount"] if 'stockCount' in tracking_payload else 0)

        # brand and subbrand detection
        for k_brand, v_subbrands in data["dictionary"].items():
            for k_subbrand, typings in v_subbrands.items():
                for typing in typings:
                    if typing.lower() in title.lower():
                        if ozon_seller_id in data['shops_ids']:
                            brand = k_brand
                            subbrand = k_subbrand

        if delivery_text == '':
            print('NO DELIVERY TEXT')
            print(result)

        if price == 0:
            print('NO PRICE')
            print(result)
        item = {
            "result_id": data["id"],
            "ts": data["ts"],
            "date_parsed": parsed_ts.date(),
            "kw_or_url": data["value"],
            "position": idx + 1,
            "position_type": "ad" if "backgroundColor" in result and result[
                'backgroundColor'] == 'ozCtrlMarketingPale' or 'advert=' in result["action"]["link"] else "seo",
            "title": title,
            "seller": seller,
            "delivery": delivery,
            "sales_points": sales_points,
            "rating": rating,
            "rating_count": rating_count,
            "link": result["action"]["link"].split("?")[0],
            "image_link": result["tileImage"]["items"][0]["image"]["link"]
            if result["tileImage"]["items"][0].get("image")
            else "False",
            "price": price,
            "oldprice": oldprice,
            "client": data["client"],
            "category": data["category"],
            "subcategory": data["subcategory"],
            "brand": brand,
            "subbrand": subbrand,
            "city": address,
            "ozon_brand": ozon_brand,
            "ozon_brandid": int(ozon_brandid),
            "ozon_seller_id": ozon_seller_id,
            "ozon_stock_count": ozon_stock_count,
            "delivery_text": delivery_text,
        }
        final_results.append(item)
    print(f"SERP DATA TO INSERT {len(final_results)}")
    return final_results


def parse_brandshelf(data):
    address = data['city']
    tracking_payloads = data['trackingPayloads']
    parsed_ts = datetime.utcnow()
    if not data['bs']:
        brandshelf_data = {
            "result_id": data["id"],
            "client": data["client"],
            "category": data["category"],
            "subcategory": data["subcategory"],
            "kw_or_url": data["value"],
            "brand": "False",
            "subbrand": "False",
            "date_parsed": parsed_ts.date(),
            "ts": data["ts"],
            "bs_title": "False",
            "bs_product_link": "False",
            "bs_product_sku": "False",
            "bs_shop": "False",
            "bs_shop_id": "False",
            "bs_brand": "False",
            "city": address,
            "ozon_brandid": 0,
            "ozon_seller_id": 0,
            "ozon_stock_count": 0,
        }
        print("BRANDSHELF DATA IS NOT AVAILABLE")
        return brandshelf_data
    results = json.loads(data['bs'])
    bs_productlink = f"/product/{results['productContainer']['products'][0]['skuId']}"
    bs_producttitle = results['productContainer']['products'][0]['alt']
    bs_title = results["header"]["title"]
    client = "Competitor"
    brand = "Competitor"
    subbrand = "Competitor"

    tracking = results["productContainer"]["products"][0]["trackingInfo"]["click"]["key"]
    ozon_brandid = 0
    ozon_seller_id = 0
    ozon_stock_count = 0

    if tracking in tracking_payloads:
        dt = json.loads(tracking_payloads[tracking])
        ozon_brandid = int(dt["brandId"] if dt.get("brandId") else 0)
        ozon_seller_id = int(dt["sellerId"] if dt.get("sellerId") else 0)
        ozon_stock_count = int(dt["stockCount"] if dt.get("stockCount") else 0)

    for k_brand, v_subbrands in data["dictionary"].items():
        for k_subbrand, typings in v_subbrands.items():
            for typing in typings:
                if typing.lower() in bs_title.lower() or typing.lower() in bs_producttitle.lower():
                    if ozon_seller_id in data['shops_ids']:
                        print("BINGO")
                        brand = k_brand
                        subbrand = k_subbrand

    brandshelf_data = {
        "result_id": data["id"],
        "client": data["client"],
        "category": data["category"],
        "subcategory": data["subcategory"],
        "kw_or_url": data["value"],
        "brand": brand,
        "subbrand": subbrand,
        "date_parsed": parsed_ts.date(),
        "ts": data["ts"],
        "bs_title": bs_title,
        "bs_product_link": bs_productlink,
        "bs_product_sku": results["productContainer"]["products"][0]["skuId"],
        "bs_shop": "False",
        "bs_shop_id": "False",
        "bs_brand": "False",
        "city": address,
        "ozon_brandid": ozon_brandid,
        "ozon_seller_id": ozon_seller_id,
        "ozon_stock_count": ozon_stock_count,
    }
    print(f"BRANDSHELF DATA TO INSERT {brandshelf_data}")
    print(brandshelf_data)
    return [brandshelf_data]


if __name__ == '__main__':
    task = {
        'id': '144f45ab-e14d-4528-9844-c126cdbbd6ad',
        'marketplace': 'ozon',
        'client': 'Кругом вода',
        'category': 'Кругом вода',
        'subcategory': 'Кругом вода',
        'type': 'search',
        'value': 'вода питьевая',
        'dictionary': {'asdasd': {'asdasd': ['asdasd']}},
        'ts': '2023-01-31 11:10:00'
    }

    data = get_html(task)

    parsed_data = parse_searchresults(data)

    parsed_brandshelf_data = parse_brandshelf(data)

    print("Done.")
