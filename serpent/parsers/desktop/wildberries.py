import re
from datetime import datetime, timezone

from bs4 import BeautifulSoup
from requests import post

from serpent.parsers.exceptions import GetAPIError


def get_html(data):
    url = f"https://www.wildberries.ru/catalog/0/search.aspx?sort=popular&search={data['value']}"
    print(f"[F:get_html] URL: {url}")
    ts = datetime.now(timezone.utc)
    try:
        headers = {
            'Cache-Control': 'no-cache',
            'Content-Type': 'application/json',
        }
        body = {
            "code": "module.exports=async({page:e,context:t})=>{let{url:a}=t;await e.goto(a),await e.evaluate(()=>{console.log(\"Element is ready\");var e=0,t=setInterval(()=>{document.querySelector(\".j-b-recommended-goods-wrapper\").scrollIntoView({behavior:\"smooth\"}),e+=1,console.log(e),11===e&&clearInterval(t)},5e2)}),await e.waitForTimeout(7e3);let o=await e.content();return{data:o,type:\"application/html\"}};",
            "context": {
                "url": url
            }
        }
        print("[F:get_html] Body:", body)
        r = post(f'http://browserless_chrome:3000/function?token=test', json=body, headers=headers)
        data['html'] = r.text
        data['ts'] = ts
    except Exception as e:
        print(e)
        raise GetAPIError(e)
    soup = BeautifulSoup(data["html"], "lxml")
    if len(soup.select("div.product-card-list > div.product-card")) == 0:
        raise GetAPIError('0 items found, retrying...')
    return data


def parse_searchresults(data: dict):
    soup: BeautifulSoup = BeautifulSoup(data["html"], "lxml")
    address = soup.find("div", id=re.compile("^state-addressBookBarWeb-"))
    items = soup.select("div.product-card-list > div.product-card")
    final_results = list()
    parsed_ts = datetime.now(timezone.utc)
    for idx, i in enumerate(items):
        (
            price,
            oldprice,
            title,
            seller,
            delivery,
            sales_points,
            rating,
            rating_count,
            brand,
            subbrand,
            wb_brand,
        ) = (0, 0, "False", "False", "False", [], 0, 0, "False", "False", "False")
        classes = i['class']
        position_type = 'ad' if 'advert-card-item' in classes else 'seo'
        try:
            title = i.select_one(":scope span.goods-name").text.strip()
        except Exception as e:
            print(e)
            print(i.prettify())
            raise Exception('cannot find title')
        for _xpath in [":scope strong.brand-name", ".brand-name"]:
            try:
                wb_brand = i.select_one(_xpath).text.strip()
            except AttributeError:
                continue
        delivery = (
            i.select_one("b.product-card__delivery-date").text.strip()
            if i.select_one("b.product-card__delivery-date") is not None
            else "False"
        )
        sales_points = (
            list(
                set(
                    [
                        c.text.strip()
                        for c in i.select("div.product-card__action > span > span")
                    ]
                )
            )
            if i.select_one("div.product-card__action") is not None
            else []
        )
        rating = (
            int(
                " ".join(
                    i.select_one(":scope span.product-card__rating")["class"]
                ).replace("product-card__rating stars-line star", "")
            )
            if i.select_one(":scope span.product-card__rating") is not None
            else 0
        )
        rating_count = (
            int(
                i.select_one(":scope span.product-card__count").text.replace("\xa0", "")
            )
            if i.select_one(":scope span.product-card__count") is not None
            else 0
        )
        price = int(
            i.select_one(":scope ins.price__lower-price")
                .text.strip()
                .replace("\u2009", "")
                .replace("₽", "")
                .replace("\xa0", "")
            if i.select_one(":scope ins.price__lower-price") is not None
            else 0
        )
        oldprice = int(
            i.select_one(":scope span.price__wrap del")
                .text.strip()
                .replace("\u2009", "")
                .replace("₽", "")
                .replace("\xa0", "")
            if i.select_one(":scope span.price__wrap del") is not None
            else 0
        )

        # brand and subbrand detection
        for k_brand, v_subbrands in data["dictionary"].items():
            for k_subbrand, typings in v_subbrands.items():
                for typing in typings:
                    if typing.lower() in title.lower():
                        brand = k_brand
                        subbrand = k_subbrand

        if i.select_one(":scope a img") is None:
            print(i)

        final_results.append(
            {
                "result_id": data["id"],
                "kw_or_url": data["value"],
                "client": data["client"],
                "category": data["category"],
                "subcategory": data["subcategory"],
                "date_parsed": parsed_ts.date(),
                "ts": data["ts"],
                "position": idx + 1,
                "position_type": position_type,
                "title": title,
                "seller": "False",
                "delivery": delivery,
                "sales_points": sales_points,
                "rating": rating,
                "rating_count": rating_count,
                "link": i.select_one(":scope a")["href"].strip(),
                "image_link": (i.select_one(":scope a img")["src"].strip()
                if not i.select_one(":scope a img").get("data-original")
                else i.select_one(":scope a img")["data-original"]) if i.select_one(":scope a img") is not None else '',
                "price": price,
                "oldprice": oldprice,
                "brand": brand,
                "subbrand": subbrand,
                "wb_brand": wb_brand,
            }
        )
    print(final_results)
    print(f"SERP DATA TO INSERT {len(final_results)}")
    return final_results


if __name__ == '__main__':
    from serpent.parsers.test_tasks import tasks
    for task in tasks:
        data = get_html(task)
        parsed_data = parse_searchresults(data)
