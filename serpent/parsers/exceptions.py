class GetAPIError(Exception):
    pass


class ScrapError(Exception):
    pass


class NoItems(ScrapError):
    pass


class LoadErrorAfterBrowserCheck(ScrapError):
    pass


class AddressFormError(ScrapError):
    pass


class UnknownTechName(Exception):
    pass
