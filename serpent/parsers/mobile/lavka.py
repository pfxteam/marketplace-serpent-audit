import os
from datetime import datetime, timezone

import requests
import urllib3

from serpent.parsers.helpers import clean_string

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
CUR_DIR = os.path.dirname(__file__)
YANDEX_PATH = os.path.join(CUR_DIR, '/')


def get_and_parse_json(data):
    results = list()
    for shop in data["addresses"]:
        headers = {
            'Cookie': 'webviewuserid_eats=z055170926594546a4f552ee73313c0f; webviewuserid=z055170926594546a4f552ee73313c0f;',
            'User-Agent': 'Mozilla/5.0 (Linux; Android 13; sdk_gphone64_x86_64 Build/TPB4.220624.004; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/101.0.4951.74 Mobile Safari/537.36 com.yandex.lavka/1.12.2.1035837 Android/13 (Google; sdk_gphone64_x86_64) Lavka/Standalone EatsKit/9.0.0'
        }
        payload = {
            "text": data['value'],
            "position": {
                "location": [
                    shop['lon'],
                    shop['lat']
                ]
            },
            "limit": 24,
            "categories_limit": 18
        }
        url = 'https://grocery-authproxy.lavka.yandex.net/4.0/eda-superapp/lavka/v1/api/v1/search'
        r = requests.post(url, json=payload, headers=headers)
        items = [p for p in r.json()['products'] if p['type'] == 'good']

        print(
            f"[F:get_and_parse_json][D:M] "
            f"Status code: {r.status_code}; "
            f"Reason: {r.reason}; "
            f"Elapsed: {r.elapsed}; "
            f"Items count: {len(items)}"
        )

        parsed_ts = datetime.now(timezone.utc)
        for idx, item in enumerate(items, start=1):
            brand, subbrand = '', ''
            for k_brand, v_subbrands in data["dictionary"].items():
                for k_subbrand, typings in v_subbrands.items():
                    for typing in typings:
                        if typing.lower() in item['title'].lower():
                            brand = k_brand
                            subbrand = k_subbrand
            results.append({
                "result_id": data["id"],
                "ts": data["ts"],
                "date_parsed": parsed_ts.date(),
                "kw_or_url": data["value"],
                "shop_address": shop['address'],
                "position": idx,
                "product_id": item['id'],
                "lavka_brand": item['options']['origin_country'],
                "title": clean_string(item['title']),
                "link": item['id'],
                "image_link": item['image_url_template'],
                "price": float(item['pricing']['price']),
                "oldprice": float(item['discount_pricing']['price'] if 'discount_pricing' in item and 'price' in item['discount_pricing'] else 0),
                "client": data["client"],
                "category": data["category"],
                "subcategory": data["subcategory"],
                "brand": brand,
                "subbrand": subbrand,
                "status": item['available'],
                "stocks": int(item['quantity_limit']),
                "device": "M"
            })
    return results


if __name__ == '__main__':
    from serpent.parsers.test_tasks import tasks
    for task in tasks:
        get_and_parse_json(data=task)
