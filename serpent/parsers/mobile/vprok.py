from datetime import datetime, timezone

import requests


def get_and_parse_json(data):

    def get_token():
        token_url = 'https://vprok.ru/mobile/api/v1/oauth/token'
        token_response = requests.post(
            url=token_url,
            json={
                "grant_type": "client_credentials_luuid",
                "client_id": 8,
                "client_secret": "ZlxUY81YlQOdWcof9G6EkHiz0316Z1Xhr0WNHDKY",
                "scope": "",
            },
            headers={
                'Host': 'www.vprok.ru',
                'User-Agent': 'PostmanRuntime/7.26.8',
            }
        )
        print(f"[F:get_token] Response text: {token_response.text}")
        token_data = token_response.json()
        return token_data['access_token']

    params = {
        'deliveryTypeId': 1,
        'page': 1,
        'limit': 20,
        'search': data['value']
    }
    headers = {
        'Authorization': f'Bearer {get_token()}',
        'Host': 'www.vprok.ru',
        'X-Api-Context-Shop-Id': '2527',
        'X-Api-Context-Region-Id': '1',
        'X-Data-Maxsyncbatchsize': '10',
        'X-Api-Context-Site-Id': '1',
        'X-Api-Context-Delivery-Type-Id': '1',
        'User-Agent': 'PostmanRuntime/7.26.8'
    }
    url = 'https://vprok.ru/mobile/api/v6/products'
    r = requests.get(url, params=params, headers=headers)
    items = r.json()['products']['items']

    print(
        f"Status code: {r.status_code}; "
        f"Reason: {r.reason}; "
        f"Elapsed: {r.elapsed}; "
        f"Items count: {len(items)}"
    )

    parsed_ts = datetime.utcnow()
    results = list()
    for idx, item in enumerate(items):
        brand, subbrand = '', ''
        for k_brand, v_subbrands in data["dictionary"].items():
            for k_subbrand, typings in v_subbrands.items():
                for typing in typings:
                    if typing.lower() in item['name'].lower():
                        brand = k_brand
                        subbrand = k_subbrand
        results.append({
            "result_id": data["id"],
            "ts": data["ts"],
            "date_parsed": parsed_ts.date(),
            "kw_or_url": data["value"],
            "position": idx + 1,
            "product_id": int(item['id']),
            "title": item['name'],
            "sales_points": [item['brightLabel']['title']] if 'brightLabel' in item else [],
            "rating": item['rating'] if 'rating' in item else 0,
            "link": str(item['id']),
            "image_link": item['image'],
            "price": float(item['price']),
            "oldprice": float(item['oldPrice']) if 'oldPrice' in item else 0,
            "client": data["client"],
            "category": data["category"],
            "subcategory": data["subcategory"],
            "brand": brand,
            "subbrand": subbrand,
            "device": "M"
        })
    return results


if __name__ == '__main__':
    from serpent.parsers.test_tasks import tasks
    for task in tasks:
        get_and_parse_json(data=task)
