from datetime import datetime, timezone

import requests


def get_and_parse_json(data):
    results = list()
    for address in data["addresses"]:
        for shop in address["shops"]:
            params = {
                'features[spellcheck]': '0',
                'per_page': '24',
                'page': '1',
                'sid': shop.get('id') or shop.get('shop_id'),
                'q': data['value']
            }
            url = 'https://api.sbermarket.ru/v2/products'
            r = requests.get(url, params=params)
            items = r.json()['products']

            print(
                f"Status code: {r.status_code}; "
                f"Reason: {r.reason}; "
                f"Elapsed: {r.elapsed}; "
                f"Items count: {len(items)}"
            )

            parsed_ts = datetime.now(timezone.utc)
            for idx, item in enumerate(items):
                brand, subbrand = '', ''
                for k_brand, v_subbrands in data["dictionary"].items():
                    for k_subbrand, typings in v_subbrands.items():
                        for typing in typings:
                            if typing.lower() in item['name'].lower():
                                brand = k_brand
                                subbrand = k_subbrand
                results.append({
                    "result_id": data["id"],
                    "ts": data["ts"],
                    "date_parsed": parsed_ts.date(),
                    "kw_or_url": data["value"],
                    "shop_name": shop['name'],
                    "shop_address": address['address'],
                    "position": idx + 1,
                    "product_id": int(item['id']),
                    "product_sku": int(item['sku']),
                    "product_retailer_sku": int(item['retailer_sku']),
                    "title": item['name'],
                    "rating": float(item['score'] if item['score'] is not None else 0),
                    "review_count": int(item['score_details']['comment_count'] if item['score_details']['comment_count'] is not None else 0),
                    "link": str(item['id']),
                    "image_link": item['images'][0]['original_url'] if item['images'] else '',
                    "sales_points": [str(b) for b in item['promo_badge_ids']],
                    "price": float(item['price']),
                    "oldprice": float(item['original_price']),
                    "client": data["client"],
                    "category": data["category"],
                    "subcategory": data["subcategory"],
                    "brand": brand,
                    "subbrand": subbrand,
                    "device": "M"
                })
    return results


if __name__ == '__main__':
    from serpent.parsers.test_tasks import tasks
    for task in tasks:
        get_and_parse_json(data=task)
