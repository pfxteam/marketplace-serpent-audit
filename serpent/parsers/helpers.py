import re
import sys
from datetime import datetime, timedelta
from os import getenv
from random import randint
from time import sleep

from clickhouse_driver import Client as ClickHouseClient
from lxml.html import Element, etree
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

from serpent.parsers.exceptions import UnknownTechName

browsers = dict()


def get_clickhouse_client() -> ClickHouseClient:
    return ClickHouseClient(getenv("DB_CLICKHOUSE_HOST", "localhost"))


def get_chrome_client():
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    options.add_argument('--incognito')
    options.add_argument('--no-first-run')
    options.add_argument('--no-service-autorun')
    options.add_argument('--password-store=basic')
    options.add_argument('--disable-web-security')
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--disable-blink-features=AutomationControlled')
    options.add_argument(
        '--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36'
        ' (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36'
    )

    # Server version
    # - - - - - - - -
    client = webdriver.Remote(
        command_executor='http://selenium_hub:4444/wd/hub',
        options=options
    )

    # # Local version
    # # - - - - - - - - -
    # client = webdriver.Chrome(
    #     executable_path="./chromedriver",
    #     chrome_options=options
    # )

    client.set_window_size(1920, 1080)
    return client


def parse_int(string):
    string = str(string)
    if re.match(r'\d+', string, re.M):
        return int(''.join([c for c in re.split(r'[,.]', string)[0] if c.isdigit()]))
    return None


def parse_float(string):
    string = str(string)
    if re.match(r'\d+', string, re.M):
        return float(''.join([x for x in string if x.isdigit() or x in [',', '.']]).replace(',', '.'))
    return None


def clean_html(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext


def format_error(e):
    return '[{e_type}] Error on line {line_number}, Message: {e_obj}'.format(
        line_number=sys.exc_info()[-1].tb_lineno,
        e_type=type(e).__name__,
        e_obj=e
    )


def wait(time_begin, from_sec: int = 5, to_sec: int = 15):
    wait_sec = randint(from_sec, to_sec)
    print(f"Waiting {wait_sec} sec..")
    while datetime.now() < time_begin + timedelta(seconds=wait_sec):
        sleep(0.6)


def lxml_get_inner_text(element: Element) -> str:
    return clean_html(etree.tostring(element, pretty_print=False, encoding="utf-8").decode('utf-8'))


def clean_string(data=None):
    if not data:
        return None

    for find_symbol, replace_to in [
        # You can find info about other characters in https://www.fileformat.info/info/unicode/char/search.htm
        (r'\u00a0', ' '),    # Latin1: NO-BREAK SPACE
        (r'\xa0', ' '),      # Latin1: NO-BREAK SPACE
        (r'\\xa0', ' '),     # Latin1: NO-BREAK SPACE
        (r'\xad', ''),       # Latin1: SOFT HYPHEN (U+00AD)
        (r'\ufeff', ''),     # Arabic Presentation Forms-B: ZERO WIDTH NO-BREAK SPACE
        (r'\u200e', ''),     # General Punctuation: LEFT-TO-RIGHT MARK
        (r'\u202a', ''),     # General Punctuation: LEFT-TO-RIGHT EMBEDDING
    ]:
        if isinstance(data, str):
            # data = data.replace(find_symbol, replace_to)
            data = re.sub(find_symbol, replace_to, data)
        else:
            # data = [s.replace(find_symbol, replace_to) for s in data]
            data = [
                re.sub(find_symbol, replace_to, s)
                for s in data
            ]

    return ' '.join([
        s for s in (
            data.split() if isinstance(data, str) else data
        )
    ]).strip("·").strip()


def check_and_get_browser_client(tech_name: str) -> dict:

    if tech_name not in ['metro', 'lavka', 'sbermarket', 'vprok']:
        raise UnknownTechName(f"Met '{tech_name} tech name. Fix it please.")

    if not browsers.get(tech_name):
        browsers[tech_name] = {
            'client': get_chrome_client()
        }
        browsers[tech_name]['client'].get("https://ya.ru")
        browsers[tech_name]['action'] = ActionChains(browsers[tech_name]['client'])
        return browsers[tech_name]
    else:
        try:
            browsers[tech_name]['client'].get("https://ya.ru")
            return browsers[tech_name]
        except:
            try:
                browsers[tech_name]['client'].quit()
                sleep(5)
            except:
                pass
            browsers[tech_name] = {
                'client': get_chrome_client()
            }
            browsers[tech_name]['client'].get("https://ya.ru")
            browsers[tech_name]['action'] = ActionChains(browsers[tech_name]['client'])
            return browsers[tech_name]
