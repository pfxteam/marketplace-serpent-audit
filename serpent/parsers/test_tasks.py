tasks = [
    {
        'id': 'bd9086c3-af99-45d6-a7d0-74e29e211504',
        'client': 'Pepsi',
        'category': 'Напитки',
        'subcategory': 'Энергетики',
        'type': 'search',
        'value': 'энергетик',
        'addresses': [
            {
                'address': 'Россия, Москва, Ленинградский проспект, 15с1',
                'lon': 37.57208167406319,
                'lat': 55.781544555606494,
                'shops': [
                    {
                        'name': 'Metro',
                        'tech_name': 'metro',
                        'shop_id': '21',
                    },
                    {
                        'name': 'Lenta',
                        'tech_name': 'lenta',
                        'shop_id': '1551',
                    }
                ]
            }
        ],
        'dictionary': {
            'PepsiCo': {
                'Adrenaline Rush': [
                    'adrenaline', 'adrenaline rush', 'adrenaline', 'adrenaline game fuel', 'adrenalin zero'
                ],
                'Drive Me': [
                    'drive me', 'драйв ми'
                ]
            },
            'Flash Up': {
                'Flash Up': ['flash up']
            },
            'Red Bull': {'Red Bull': ['red bull']},
            'The Scandalist Energy Drink': {'The Scandalist Energy Drink': ['scandalist energy drink']},
            'Monster': {'Monster': ['monster', 'монстер']}, 'М-150': {'М-150': ['м-150']},
            'Таргет': {'Таргет': ['таргет']}, 'ALLIGATOR': {'ALLIGATOR': ['alligator', 'alligator']},
            'Fitness Food Factory': {'Fitness Food Factory WK UP': ['fitness food factory wk up']},
            'IMBA ENERGY': {'IMBA ENERGY': ['imba energy']}},
        'ts': '2022-12-09 08:00:05'
    },
    {
        'id': 'bd9086c3-af99-45d6-a7d0-74e29e211504',
        'client': 'Pepsi',
        'category': 'Напитки',
        'subcategory': 'Энергетики',
        'addresses': [
            {
                'address': 'Россия, Москва, Ленинградский проспект, 15с1',
                'lon': 37.57208167406319,
                'lat': 55.781544555606494,
                'shops': [
                    {
                        'name': 'Metro',
                        'tech_name': 'metro',
                        'shop_id': '21',
                    },
                    {
                        'name': 'Lenta',
                        'tech_name': 'lenta',
                        'shop_id': '1551',
                    }
                ]
            }
        ],
        'type': 'search',
        'value': 'пиво',
        'dictionary': {},
    },
    {
        'id': 'bd9086c3-af99-45d6-a7d0-74e29e211504',
        'client': 'Pepsi',
        'category': 'Напитки',
        'subcategory': 'Энергетики',
        'addresses': [
            {
                'address': 'Россия, Москва, Ленинградский проспект, 15с1',
                'lon': 37.57208167406319,
                'lat': 55.781544555606494,
                'shops': [
                    {
                        'name': 'Metro',
                        'tech_name': 'metro',
                        'shop_id': '21',
                    },
                    {
                        'name': 'Lenta',
                        'tech_name': 'lenta',
                        'shop_id': '1551',
                    }
                ]
            }
        ],
        'type': 'search',
        'value': 'паста',
        'dictionary': {},
    },
]
