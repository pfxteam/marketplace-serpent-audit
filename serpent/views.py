import json
from uuid import uuid4

from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Q, Count
from django.db.utils import IntegrityError
from django.http import JsonResponse
from django.shortcuts import render, redirect, HttpResponseRedirect
from django.utils.safestring import SafeString

from marketplace_serpent import settings
from serpent.dataclasses import ProjectData, GeoData
from serpent.forms import LoginUsersForms, GeoCreateForm, CreateAndEditKeywordGroupForm
from serpent.models.engine import UserProfile
from serpent.models.serpent import Client, Project, Geo, Marketplace, Keyword, KeywordGroup, Shop

MANAGE_PAGE = '/manage/'
LOGIN_URL = settings.LOGIN_URL


def user_login(request):
    error_text = False
    next_url = request.GET.get('next', MANAGE_PAGE) or MANAGE_PAGE

    if request.user.is_authenticated:
        return HttpResponseRedirect(next_url)

    if request.method == 'POST':

        form = LoginUsersForms(request.POST)
        if form.is_valid():
            login_name = form.cleaned_data['login']
            password = form.cleaned_data['password']
            user_auth = authenticate(
                username=login_name,
                password=password
            )
            if user_auth:
                login(request, user_auth)
                return HttpResponseRedirect(next_url)
            else:
                error_text = 'Incorrect login or password.'
                messages.add_message(request, messages.ERROR, error_text)
                return redirect(LOGIN_URL)
        else:
            error_text = 'Not all fields are filled out'
            messages.add_message(request, messages.ERROR, error_text)
            return redirect(LOGIN_URL)
    else:
        form = LoginUsersForms()

    return render(
        request=request,
        template_name='login.html',
        context={'form': form, 'error_text': error_text}
    )


@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(LOGIN_URL)


@login_required
def index(request):
    return HttpResponseRedirect(MANAGE_PAGE)


@login_required
def admin(request):
    if request.user.is_staff:
        clients = Client.objects.all()
    else:
        clients = Client.objects.filter(
            Q(owner=request.user) | Q(shares=request.user) | Q(shares_edit=request.user)
        ).distinct()

    clients_data = clients.prefetch_related(
        'project_set', 'shares', 'shares_edit', 'project_set__keywordgroup_set'
    ).values(
        'project', 'project__keywordgroup'
    ).annotate(
        kw_count=Count('project__keywordgroup__keywords', distinct=True)
    ).values(
        'id',
        'name',
        'owner',
        'owner__username',
        'shares',
        'shares_edit',
        'is_active',
        'project',
        'project__name',
        'project__screen',
        'project__is_active',
        'project__keywordgroup',
        'project__keywordgroup__name',
        'project__keywordgroup__is_active',
        'kw_count',
        'kw_limit'
    )

    users = User.objects.filter(is_staff=False)

    data = dict()
    for client in clients_data:
        if client['id'] not in data:
            data[client['id']] = {
                'id': client['id'],
                'name': client['name'],
                'owner': client['owner'],
                'owner_name': client['owner__username'],
                'shares': [client['shares']],
                'shares_edit': [client['shares_edit']],
                'is_active': client['is_active'],
                'kw_limit': client['kw_limit'],
                'projects': {
                    client['project']: {
                        'id': client['project'],
                        'name': client['project__name'],
                        'screen': client['project__screen'],
                        'is_active': client['project__is_active'],
                        'kwgs_count': 1 if client.get('project__keywordgroup') else 0,
                        'kwgs': {
                            client['project__keywordgroup']: {
                                'id': client['project__keywordgroup'],
                                'name': client['project__keywordgroup__name'],
                                'kw_count': client['kw_count'],
                                'is_active': client['project__keywordgroup__is_active']
                            }
                        } if client.get('project__keywordgroup') else {},
                    }
                }
            } if client.get('project') else {}
        elif client.get('shares') and client['shares'] not in data[client['id']]['shares']:
            data[client['id']]['shares'].append(client['shares'])
        elif client.get('shares_edit') and client['shares_edit'] not in data[client['id']]['shares_edit']:
            data[client['id']]['shares_edit'].append(client['shares_edit'])
        elif client.get('project') and client['project'] not in data[client['id']]['projects']:
            data[client['id']]['projects'][client['project']] = {
                'id': client['project'],
                'name': client['project__name'],
                'screen': client['project__screen'],
                'is_active': client['project__is_active'],
                'kwgs_count': 1 if client.get('project__keywordgroup') else 0,
                'kwgs': {
                    client['project__keywordgroup']: {
                        'id': client['project__keywordgroup'],
                        'name': client['project__keywordgroup__name'],
                        'kw_count': client['kw_count'],
                        'is_active': client['project__keywordgroup__is_active']
                    }
                } if client.get('project__keywordgroup') else {},
            }
        elif client.get('project__keywordgroup') and \
                client['project__keywordgroup'] not in data[client['id']]['projects'][client['project']]['kwgs']:
            data[client['id']]['projects'][client['project']]['kwgs'][client['project__keywordgroup']] = {
                'id': client['project__keywordgroup'],
                'name': client['project__keywordgroup__name'],
                'kw_count': client['kw_count'],
                'is_active': client['project__keywordgroup__is_active']
            }
            data[client['id']]['projects'][client['project']]['kwgs_count'] += 1

    for client_id in data:
        data[client_id]['total_kw'] = Keyword.objects.filter(
            keywordgroup__project__client_id=client_id,
        ).distinct().count()
        clients_active_kwgs = KeywordGroup.objects.filter(project__client_id=client_id, is_active=True)
        data[client_id]['total_active_kw'] = Keyword.objects.filter(
            keywordgroup__in=clients_active_kwgs
        ).distinct().count()
        for project_id in data[client_id]['projects']:
            data[client_id]['projects'][project_id]['total_kw'] = Keyword.objects.filter(
                keywordgroup__project_id=project_id,
            ).distinct().count()
            projects_active_kwgs = KeywordGroup.objects.filter(project_id=project_id, is_active=True)
            data[client_id]['projects'][project_id]['total_active_kw'] = Keyword.objects.filter(
                keywordgroup__in=projects_active_kwgs
            ).distinct().count()
            data[client_id]['projects'][project_id]['kwgs'] = list(
                data[client_id]['projects'][project_id]['kwgs'].values()
            )
        data[client_id]['projects'] = list(
            data[client_id]['projects'].values()
        )

    data = list(data.values())

    data = {'data': data, 'users': users}

    return render(request, 'serpent/admin-page.html', data)


@login_required
def add_project(request):
    if request.method == "POST":
        data = ProjectData(
            client_id=request.POST.get('client_id'),
            client_name=request.POST.get('new_client_name'),
            brandshelf=request.POST.get('brandshelf'),
            name=request.POST.get('project_name'),
            description=request.POST.get('project_description'),
            is_active=request.POST.get('is_active'),
            crontab=request.POST.get('crontab'),
            screen=request.POST.get('screen'),
            category=request.POST.get('category'),
            subcategory=request.POST.get('subcategory'),
            marketplace=request.POST.get('marketplace'),
            shops=request.POST.get('shops'),
            geo_id=request.POST.get('geo_id'),
        )

        if data.screen not in [s[0] for s in Project.SCREENS]:
            messages.add_message(
                request,
                messages.ERROR,
                f'Invalid screen ({data.screen})'
            )
            return JsonResponse({
                'status': False,
                'message': f'Invalid screen ({data.screen})'
            }, status=400)

        if data.client_name:
            user = UserProfile.objects.get(user=request.user)
            usage, limit = user.get_client_active_usage(), user.client_limit
            if usage >= limit:
                messages.add_message(
                    request,
                    messages.ERROR,
                    f'You reached clients limit: {usage} / {limit} used'
                )
                return JsonResponse({
                    'status': False,
                    'message': f'You reached clients limit: {usage} / {limit} used'
                }, status=400)
            try:
                client = Client.objects.create(
                    name=data.client_name,
                    owner=request.user
                )
            except IntegrityError:
                messages.add_message(
                    request,
                    messages.ERROR,
                    f'Duplicate Client name ({data.client_name}) or owner ({request.user})'
                )
                return JsonResponse({
                    'status': False,
                    'message': f'Duplicate Client name ({data.client_name}) or owner ({request.user})'
                }, status=400)
        else:
            try:
                client = Client.objects.get(pk=data.client_id)
            except Project.DoesNotExist:
                messages.add_message(
                    request,
                    messages.ERROR,
                    f'Invalid client id: {data.client_id}'
                )
                return JsonResponse({
                    'status': False,
                    'message': f'Invalid client id: {data.client_id}'
                }, status=400)

        try:
            geo = Geo.objects.get(id=data.geo_id)
        except Project.DoesNotExist:
            messages.add_message(
                request,
                messages.ERROR,
                f'Invalid geo id: {data.geo_id}'
            )
            return JsonResponse({
                'status': False,
                'message': f'Invalid geo id: {data.geo_id}'
            }, status=400)

        try:
            marketplace = Marketplace.objects.get(tech_name=data.marketplace)
        except Project.DoesNotExist:
            messages.add_message(
                request,
                messages.ERROR,
                f'Invalid marketplace: {data.marketplace}'
            )
            return JsonResponse({
                'status': False,
                'message': f'Invalid marketplace: {data.marketplace}'
            }, status=400)

        if request.user.is_staff or request.user == client.owner or request.user in client.shares_edit.all():
            try:
                new_project = Project.objects.create(
                    uuid=str(uuid4()),
                    client=client,
                    name=data.name,
                    crontab=data.crontab if data.crontab else '0 * * * *',
                    description=data.description or '- - -',
                    is_active=data.is_active,
                    category=data.category,
                    subcategory=data.subcategory,
                    geo=geo,
                    screen=data.screen,
                    marketplace=marketplace,
                    brandshelf=data.brandshelf
                )
                if data.shops:
                    for shop_id in data.shops:
                        new_project.shops.add(Shop.objects.get(id=shop_id))
                    new_project.save()
            except IntegrityError:
                messages.add_message(
                    request,
                    messages.ERROR,
                    f'Duplicate project name: {data.name}'
                )

                return JsonResponse({
                    'status': False,
                    'message': f'Duplicate project name: {data.name}'
                }, status=400)
        else:
            messages.add_message(
                request,
                messages.ERROR,
                f'Operation not permitted'
            )
            return JsonResponse({
                'status': False,
                'message': f'Operation not permitted'
            }, status=403)

        messages.add_message(
            request,
            messages.SUCCESS,
            f'Project "{data.name}" has been created!'
        )
        return JsonResponse({
            'status': True,
            'message': f'Project "{data.name}" has been created!'
        }, status=200)

    # GET method
    clients = Client.objects.all() if request.user.is_staff else Client.objects.filter(
        Q(owner=request.user) | Q(shares_edit=request.user)
    ).distinct()
    geos = Geo.objects.all().values('id', 'name')
    marketplaces = list(Marketplace.objects.all().values('tech_name', 'name'))
    marketplaces = sorted(marketplaces, key=lambda x: x['tech_name'])
    shops = [
        {
            "mp_tech_name": x['marketplace__tech_name'],
            "mp_id": x['marketplace__id'],
            "shop_id": x['id'],
            "shop_name": x['name']
        }
        for x in Shop.objects.all().values('id', 'name', 'marketplace__id', 'marketplace__tech_name')
    ]
    return render(
        request=request,
        template_name='serpent/admin_add_project.html',
        context={
            'clients': clients,
            'screens': Project.SCREENS,
            'geos': geos,
            'marketplaces': marketplaces,
            'shops': shops,
            'brandshelfShow': SafeString(json.dumps({
                'ozon': True,
            })),
            'shopsShow': SafeString(json.dumps(shops))
        }
    )


@login_required
def edit_project(request, project_id):
    try:
        project = Project.objects.get(id=project_id)
    except Project.DoesNotExist:
        messages.add_message(
            request,
            messages.ERROR,
            f"No project with project_id: {project_id}!"
        )
        return redirect('/manage/')

    if request.user.is_staff or \
            project.client.owner == request.user or \
            request.user in project.client.shares_edit.all():
        if request.method == "POST":
            print(repr(request.POST))

            data = ProjectData(
                client_id=request.POST.get('client_id'),
                client_name=request.POST.get('new_client_name'),
                brandshelf=request.POST.get('brandshelf'),
                name=request.POST.get('project_name'),
                description=request.POST.get('project_description'),
                is_active=request.POST.get('is_active'),
                crontab=request.POST.get('crontab'),
                screen=request.POST.get('screen'),
                category=request.POST.get('category'),
                subcategory=request.POST.get('subcategory'),
                marketplace=request.POST.get('marketplace'),
                shops=request.POST.getlist('shops'),
                geo_id=request.POST.get('geo_id'),
            )

            try:
                project = Project.objects.get(id=project_id)
            except Project.DoesNotExist:
                messages.add_message(
                    request,
                    messages.ERROR,
                    f'No project with project id: {project_id}!'
                )
                return redirect(request.META.get('HTTP_REFERER', 'Admin page'))

            project.name = data.name
            project.description = data.description
            project.crontab = data.crontab
            project.screen = data.screen
            project.category = data.category
            project.subcategory = data.subcategory
            project.geo = Geo.objects.get(id=data.geo_id)
            project.is_active = data.is_active
            project.brandshelf = data.brandshelf
            project.shops.clear()
            for shop in data.shops:
                project.shops.add(Shop.objects.get(id=shop))
            project.save()

            messages.add_message(
                request,
                messages.SUCCESS,
                f'Project "{data.name}" has been updated!'
            )
            return redirect('/manage/')

        # GET method
        clients = Client.objects.all() if request.user.is_staff else Client.objects.filter(
            Q(owner=request.user) | Q(shares_edit=request.user)
        ).distinct()
        geos = Geo.objects.all().values('id', 'name')
        marketplaces = [(x['id'], x['name']) for x in Marketplace.objects.all().values('id', 'name')]
        shops = Shop.objects.filter(marketplace=project.marketplace)

        return render(
            request=request,
            template_name='serpent/admin_edit_project.html',
            context={
                'clients': clients,
                'project': project,
                'screens': Project.SCREENS,
                'geos': geos,
                'marketplaces': marketplaces,
                'shops': shops
            }
        )

    else:
        messages.add_message(
            request,
            messages.ERROR,
            f'Operation not permitted'
        )
        return redirect(f'/manage/')


@login_required
def add_geo(request):
    if request.method == "POST":
        data = GeoData(
            name=request.POST.get('name'),
            country=request.POST.get('country'),
            city=request.POST.get('city'),
            street=request.POST.get('street'),
            house=request.POST.get('house'),
            lon=request.POST.get('longitude'),
            lat=request.POST.get('latitude'),
        )

        print(repr(data))
        try:
            Geo.objects.create(
                name=data.name,
                country=data.country,
                city=data.city,
                street=data.street,
                house=data.house,
                lon=data.lon,
                lat=data.lat
            )
            messages.add_message(
                request,
                messages.SUCCESS,
                f'Geo "{data.name}" has been created!'
            )
            return redirect('/manage/')
        except IntegrityError:
            messages.add_message(
                request,
                messages.ERROR,
                f"Geo hasn't been created. Try again, please."
            )
            return redirect(request.META.get('HTTP_REFERER', 'Admin page'))

    form = GeoCreateForm()
    return render(
        request=request,
        template_name='serpent/admin_add_geo.html',
        context={'form': form}
    )


@login_required
def add_kw_group(request, project_id):
    try:
        project = Project.objects.get(pk=project_id)
    except Project.DoesNotExist:
        messages.add_message(
            request,
            messages.ERROR,
            f'Invalid project id ({project_id}), project does not exists.'
        )
        return redirect(f'/manage/')
    if request.user.is_staff or \
            project.client.owner == request.user or \
            request.user in project.client.shares_edit.all():
        if request.method == 'POST':
            keywords = (lambda _kws: [_k.strip() for _k in _kws if _k.strip()])(
                request.POST.get('kwg_keywords').strip().split('\r\n')
            )

            # Create keyword group
            kwg_name = request.POST.get('kwg_name')
            try:
                kwg = KeywordGroup.objects.create(
                    name=kwg_name,
                    project=project
                )
            except IntegrityError:
                messages.add_message(
                    request,
                    messages.ERROR,
                    f'Duplicate keyword group name: "{kwg_name}"!'
                )
                return redirect(f'manage/project/{project_id}/kwg/add/')

            # Create or get exists keywords and add them all into a new keyword group
            kws = []
            for kw in keywords:
                _kw = Keyword.objects.get_or_create(keyword=kw)[0]
                kws.append(_kw)

            kwg.keywords.add(*kws)
            kwg.save()

            # Success notification
            messages.add_message(
                request,
                messages.SUCCESS,
                f'A new keyword group "{kwg.name}" has been created!'
            )
            return redirect(f'/manage/')

        # GET method
        form = CreateAndEditKeywordGroupForm()
        return render(
            request=request,
            template_name='serpent/admin_add_kw_group.html',
            context={
                'title': "Add new keyword group",
                'edit_mode': False,
                'form': form,
                'kw_limit': project.client.kw_limit,
                'kw_usage': project.client.get_active_kws().count(),
                'kwg_active': False
            }
        )

    else:
        messages.add_message(
            request,
            messages.ERROR,
            f'Operation not permitted'
        )
        return redirect(f'/manage/')


@login_required
def edit_kw_group(request, project_id, kwg_id):
    try:
        kwg = KeywordGroup.objects.get(
            project__id=project_id,
            pk=kwg_id
        )
    except KeywordGroup.DoesNotExist:
        messages.add_message(
            request,
            messages.ERROR,
            f"No keyword group with project_id: {project_id} and kwg's id: {kwg_id}!"
        )

        return redirect('/manage/')

    if request.user.is_staff or \
            kwg.project.client.owner == request.user or \
            request.user in kwg.project.client.shares_edit.all():
        if request.method == "POST":
            keywords = set(request.POST.get('kwg_keywords').strip().split('\r\n'))
            if kwg.is_active:
                other_kwgs = KeywordGroup.objects.filter(project__client=kwg.project.client, is_active=True) \
                    .exclude(id=kwg.id)
                new_keywords = keywords - set(kwg.project.client.get_active_kws().values_list('keyword', flat=True))
                keywords_to_remove = set(
                    kwg.keywords.values_list('keyword', flat=True)
                ) - keywords - set(
                    Keyword.objects.filter(keywordgroup__in=other_kwgs).values_list('keyword', flat=True)
                )
                kw_cnt_after = kwg.project.client.get_active_kws().count() - len(keywords_to_remove) + len(new_keywords)
                if kwg.is_active and kw_cnt_after > kwg.project.client.kw_limit:
                    limit_diff = kw_cnt_after - kwg.project.client.kw_limit
                    messages.add_message(
                        request,
                        messages.ERROR,
                        f'Your limit is reached. Remove {limit_diff} keywords or deactivate this keyword group'
                    )
                    return redirect(f'/manage/project/{project_id}/kwg/{kwg_id}/')

            kwg_name = request.POST.get('kwg_name')
            try:
                kwg.name = kwg_name
                kwg.save()
            except IntegrityError:
                messages.add_message(
                    request,
                    messages.ERROR,
                    f'Duplicated project name: {kwg_name}'
                )

                return redirect(f'/manage/project/{project_id}/kwg/{kwg_id}/edit/')

            old_kws = set(kwg.keywords.all().values_list('id', flat=True))
            kwg.keywords.clear()
            for kw in keywords:
                keyword = Keyword.objects.get_or_create(keyword=kw)[0]
                if keyword.is_active != kwg.is_active:
                    keyword.is_active = kwg.is_active
                    keyword.save()
                kwg.keywords.add(keyword)
            kwg.save()

            Keyword.objects.filter(id__in=old_kws, keywordgroup=None).delete()
            Keyword.objects.filter(id__in=old_kws).exclude(keywordgroup__is_active=True) \
                .distinct().update(is_active=False)

            # Success notification
            messages.add_message(
                request,
                messages.SUCCESS,
                'Saved.'
            )

            return redirect(f'/manage/project/{project_id}/kwg/{kwg_id}/')

        # GET method
        form = CreateAndEditKeywordGroupForm({
            'kwg_name': kwg.name,
            'kwg_keywords': '\r\n'.join([kw.keyword for kw in kwg.keywords.all()])
        })
        return render(
            request=request,
            template_name='serpent/admin_add_kw_group.html',
            context={
                'title': f"'{kwg.name}' keyword group editor",
                'kwg': kwg,
                'edit_mode': True,
                'form': form,
                'kw_limit': kwg.project.client.kw_limit,
                'kw_usage': kwg.project.client.get_active_kws().count(),
                'kwg_active': kwg.is_active
            }
        )

    else:
        messages.add_message(
            request,
            messages.ERROR,
            f'Operation not permitted'
        )

        return redirect(f'/manage/')


@login_required
def view_kw_group(request, project_id, kwg_id):
    try:
        project = Project.objects.get(pk=project_id)
    except Project.DoesNotExist:
        messages.add_message(
            request,
            messages.ERROR,
            f'Invalid project_id: {project_id}'
        )
        return redirect(f'/manage/')

    if request.user.is_staff or \
            project.client.owner == request.user or \
            request.user in project.client.shares.all() or \
            request.user in project.client.shares_edit.all():
        try:
            kwg = KeywordGroup.objects.get(
                project=project,
                pk=kwg_id
            )
        except KeywordGroup.DoesNotExist:
            messages.add_message(
                request,
                messages.ERROR,
                f'Invalid project ({project_id}) or kwg_id ({kwg_id})'
            )
            return redirect(f'/manage/')

        return render(request, 'serpent/admin_view_kw_group.html', {
            'kwg': kwg,
            'keywords': [_kw['keyword'] for _kw in kwg.keywords.all().values('keyword')]
        })

    messages.add_message(
        request,
        messages.ERROR,
        f'Operation not permitted'
    )
    return redirect('/manage/')


@login_required
def admin_edit_active(request, model, obj_id):
    if request.method == 'POST':
        try:
            # Client
            if model == "client":
                obj = Client.objects.get(pk=obj_id)
                obj_type = "Client"
                owner = obj.owner
                client = obj
                shared_user_edit_enable = request.user in obj.shares_edit.all()

            # Project
            elif model == "project":
                obj = Project.objects.get(pk=obj_id)
                obj_type = "Project"
                owner = obj.client.owner
                client = obj.client
                shared_user_edit_enable = request.user in obj.client.shares_edit.all()

            # Keyword Group
            elif model == "kwg":
                obj = KeywordGroup.objects.get(pk=obj_id)
                obj_type = "Keyword group"
                owner = obj.project.client.owner
                client = obj.project.client
                shared_user_edit_enable = request.user in obj.project.client.shares_edit.all()

            else:
                return JsonResponse({'error': f'Invalid model type with id: {model} {obj_id}'}, status=400)

        except (
                Client.DoesNotExist,
                Project.DoesNotExist,
                KeywordGroup.DoesNotExist,
                ValueError,
                TypeError
        ):
            return JsonResponse({'error': f'Invalid {model} id: {obj_id}'}, status=400)

        if request.user.is_staff or owner == request.user or shared_user_edit_enable:
            try:
                active = request.POST['active']
            except KeyError:
                return JsonResponse({'error': f'Invalid "active" param'}, status=400)

            if active == 'true':
                active = True
            elif active == 'false':
                active = False
            else:
                return JsonResponse({'error': f'Invalid "active" param, must be true/false, got "{active}"'},
                                    status=400)

            if obj.is_active != active:
                if active:
                    if not client.is_active:
                        user = UserProfile.objects.get(
                            user=owner if shared_user_edit_enable else request.user
                        )
                        client_usage, client_limit = user.get_client_active_usage(), user.client_limit

                        if client_usage >= client_limit:
                            return JsonResponse({
                                'error': f'You have reached your clients limit. {client_usage} / {client_limit}'
                            }, status=400)

                    if model == 'client':
                        client_kw_usage, client_kw_limit = client.get_kws().count(), client.kw_limit

                        if client_kw_usage > client_kw_limit:
                            return JsonResponse({
                                'error': f'You have reached your keywords limit for this client. '
                                         f'{client_kw_usage} / {client_kw_limit}'
                            }, status=400)

                    elif model == 'project':
                        other_projects = Project.objects.filter(client=client, is_active=True).exclude(id=obj.id)
                        other_kwgs = KeywordGroup.objects.filter(project__in=other_projects, is_active=True)
                        new_keywords = Keyword.objects.filter(
                            keywordgroup__in=obj.keywordgroup_set.all()
                        ).exclude(
                            keywordgroup__in=other_kwgs
                        ).distinct()
                        project_kw_usage, client_kw_limit = client.get_active_kws().count() + new_keywords.count(), client.kw_limit

                        if project_kw_usage > client_kw_limit:
                            return JsonResponse({
                                'error': f'You have reached your keywords limit for this client. '
                                         f'{project_kw_usage} / {client_kw_limit}'
                            }, status=400)

                    elif model == "kwg":
                        other_kwgs = KeywordGroup.objects.filter(project__client=client, is_active=True)
                        new_keywords = Keyword.objects.filter(
                            keywordgroup=obj
                        ).exclude(
                            keywordgroup__in=other_kwgs
                        ).distinct()
                        kw_cnt_after, limit = new_keywords.count() + client.get_active_kws().count(), client.kw_limit

                        if kw_cnt_after > limit:
                            limit_diff = kw_cnt_after - client.kw_limit
                            return JsonResponse({
                                'error': f'You have {limit_diff} keywords over limit. {kw_cnt_after} / {limit}'
                            }, status=400)

                obj.is_active = active
                obj.save()

            return JsonResponse({
                'ok': f'{obj_type} "{obj.name}" is ACTIVATED'
                if active else f'{obj_type} "{obj.name}" is DEACTIVATED'
            })

    return redirect('/manage/')


@login_required
def admin_delete_object(request, object_type, object_id):
    if request.method == 'POST':
        try:
            if object_type == "client":
                _object = Client.objects.get(pk=object_id)
                _owner = _object.owner
            elif object_type == "project":
                _object = Project.objects.get(pk=object_id)
                _owner = _object.client.owner
            elif object_type == "kwg":
                _object = KeywordGroup.objects.get(pk=object_id)
                _owner = _object.project.client.owner
            else:
                return JsonResponse(
                    {'error': f'Invalid object_type: {object_type}. Correct object types are: client, project, kwg.'},
                    status=400
                )

            if request.user.is_staff or _owner == request.user:
                _object.delete()
                return JsonResponse({
                    'ok': f'"{_object.name}" {object_type} has been deleted.'
                })

        except (Client.DoesNotExist, Project.DoesNotExist, KeywordGroup.DoesNotExist, ValueError, TypeError):
            return JsonResponse({'error': f'Invalid object_id: {object_id} (object_type: {object_type})'}, status=400)

    return redirect('/manage/')


@login_required
def admin_edit_shares(request, client_id):
    if request.method == 'POST':

        try:
            client = Client.objects.get(pk=client_id)
        except (Client.DoesNotExist, ValueError, TypeError):
            return JsonResponse({'error': f'Invalid client_id: {client_id}'}, status=400)

        if request.user.is_staff or client.owner == request.user:

            try:
                user_list = request.POST['users'].split(',')
                editable = True if request.POST['editable'] == "true" else False
            except KeyError:
                return JsonResponse({'error': f'Invalid "users" list'}, status=400)

            shares_object = client.shares_edit if editable else client.shares
            shares_backup = [user.id for user in shares_object.all()]

            shares_object.clear()
            usernames = set()

            for user_id in user_list:
                if user_id:

                    try:
                        user = User.objects.get(id=user_id)
                    except (User.DoesNotExist, ValueError, TypeError):
                        shares_object.clear()
                        for u_id in shares_backup:
                            try:
                                user = User.objects.get(id=u_id)
                            except (User.DoesNotExist, ValueError, TypeError):
                                continue
                            if not user.is_staff:
                                shares_object.add(user)
                        return JsonResponse({'error': f'Invalid user_id: {user_id}'}, status=400)

                    usernames.add(user.username)
                    shares_object.add(user)
            return JsonResponse({
                'ok': f'Client "{client.name}" is now sharing with: {", ".join(usernames)}'
                if usernames else f'Client "{client.name}" is NOT sharing now'
            })
        else:
            return JsonResponse({'error': f'You are not owner of Client (id: {client_id})'}, status=403)

    return redirect('/manage/')

