from django.apps import AppConfig


class SerpentConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'serpent'

    def ready(self):
        import serpent.signals
