from django.urls import path

from . import views

urlpatterns = [
    path('', views.index),
    path('login/', views.user_login, name='user_login'),
    path('logout/', views.user_logout, name='user_logout'),
    path('manage/', views.admin, name='serpent_admin'),
    path('manage/project/add/', views.add_project, name='add_project'),
    path('manage/project/<int:project_id>/edit/', views.edit_project, name='edit_project'),
    path('manage/geo/add/', views.add_geo, name='add_geo'),
    path('manage/project/<int:project_id>/kwg/add/', views.add_kw_group, name='add_kw_group'),
    path('manage/project/<int:project_id>/kwg/<int:kwg_id>/edit/', views.edit_kw_group, name='edit_kw_group'),
    path('manage/project/<int:project_id>/kwg/<int:kwg_id>/', views.view_kw_group, name='view_kw_group'),

    # Edit client, project or keyword group activity
    path('manage/admin_edit_active/<str:model>/<int:obj_id>/', views.admin_edit_active, name="Toggle object activity"),
    # Delete client, project, url group or keyword group
    path(
        'manage/admin_delete_object/<str:object_type>/<int:object_id>/',
        views.admin_delete_object,
        name="Admin Delete object"
    ),
    # Edit clients shares
    path('manage/admin_edit_shares/<int:client_id>/', views.admin_edit_shares, name="admin_edit_shares"),
]
