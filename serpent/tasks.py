import datetime
from json import loads

import pytz

from marketplace_serpent.celery import app
from serpent.models.serpent import KeywordGroup, Project
from serpent.parsers.desktop import lavka as lavka_desktop_parser
from serpent.parsers.desktop import metro as metro_desktop_parser
from serpent.parsers.desktop import ozon as ozon_desktop_parser
from serpent.parsers.desktop import sbermarket as sbermarket_desktop_parser
from serpent.parsers.desktop import vprok as vprok_desktop_parser
from serpent.parsers.desktop import wildberries as wildberries_desktop_parser
from serpent.parsers.helpers import get_clickhouse_client
from serpent.parsers.mobile import lavka as lavka_mobile_parser
from serpent.parsers.mobile import metro as metro_mobile_parser
from serpent.parsers.mobile import sbermarket as sbermarket_mobile_parser
from serpent.parsers.mobile import vprok as vprok_mobile_parser

PARSERS = {
    'D': {
        'metro': metro_desktop_parser,
        'lavka': lavka_desktop_parser,
        'vprok': vprok_desktop_parser,
        'sbermarket': sbermarket_desktop_parser,
        'wildberries': wildberries_desktop_parser,
        'ozon': ozon_desktop_parser,
    },
    'M': {
        'metro': metro_mobile_parser,
        'lavka': lavka_mobile_parser,
        'vprok': vprok_mobile_parser,
        'sbermarket': sbermarket_mobile_parser,
    }
}

ADDRESS_FORMATS = [
    "{country}, {city}, {street}, {house}",  # 0
    "{street}, {house}, {city}, {country}",  # 1
    "{street} {house}",                      # 2
    "{street}, {house}",                     # 3
]

ADDRESSES = {
    'lavka': {
        'D': ADDRESS_FORMATS[0],
        'M': ADDRESS_FORMATS[2],
    },
    'metro': {
        'D': ADDRESS_FORMATS[1],
        'M': ADDRESS_FORMATS[2],
    },
    'sbermarket': {
        'D': ADDRESS_FORMATS[1],
        'M': ADDRESS_FORMATS[2],
    },
    'vprok': {
        'D': ADDRESS_FORMATS[0],
        'M': ADDRESS_FORMATS[2],
    }
}


@app.task(
    name='MP_SERPENT_wildberries',
    retry_kwargs={'max_retries': 5, 'countdown': 5},
    retry_backoff=True,
    autoretry_for=(Exception,),
    queue='wildberries',
    ignore_result=True,
)
def task_wildberries(task):
    parser = PARSERS[task['screen']][task['marketplace']]
    clickhouse_client = get_clickhouse_client()
    try:
        data = parser.get_html(task)
        parsed_data = parser.parse_searchresults(data)
        clickhouse_client.execute(
            "INSERT INTO wb_v2 "
            "(result_id, ts, date_parsed, kw_or_url, position, position_type, title, seller, delivery, sales_points, "
            "rating, rating_count, link, image_link, price, oldprice, client, category, subcategory, brand, subbrand, "
            "wb_brand) VALUES",
            parsed_data,
        )
    except Exception as e:
        del clickhouse_client
        try:
            del data
            del parsed_data
        except NameError:
            pass
        raise e
    print("All ok. Finishing...")
    del clickhouse_client
    del data
    del parsed_data
    del parser
    print("Bye!")


@app.task(
    name='MP_SERPENT_ozon',
    retry_kwargs={'max_retries': 5, 'countdown': 5},
    retry_backoff=True,
    autoretry_for=(Exception,),
    queue='ozon',
    ignore_result=True,
)
def task_ozon(task):
    parser = PARSERS[task['screen']][task['marketplace']]
    clickhouse_client = get_clickhouse_client()
    try:
        data = parser.get_html(task)
        parsed_data = parser.parse_searchresults(data)
        clickhouse_client.execute(
            "INSERT INTO ozon "
            "(result_id, ts, date_parsed, kw_or_url, position, position_type, title, seller, delivery, sales_points, "
            "rating, rating_count, link, image_link, price, oldprice, client, category, subcategory, brand, subbrand, "
            "city, ozon_brand, ozon_brandid, ozon_seller_id, ozon_stock_count, delivery_text) VALUES",
            parsed_data,
        )
        if task["brandshelf"]:
            parsed_brandshelf_data = parser.parse_brandshelf(data)
            clickhouse_client.execute(
                "INSERT INTO ozon_brandshelf "
                "(result_id, client, category, subcategory, kw_or_url, brand, subbrand, date_parsed, ts, bs_title, "
                "bs_product_link, bs_product_sku, bs_shop, bs_shop_id, bs_brand, city, ozon_brandid, ozon_seller_id, "
                "ozon_stock_count) VALUES",
                parsed_brandshelf_data,
            )
    except Exception as e:
        del clickhouse_client
        try:
            del data
            del parsed_data
        except NameError:
            pass
        raise e
    print("All ok. Finishing...")
    del clickhouse_client
    del data
    del parsed_data
    del parsed_brandshelf_data
    del parser
    print("Bye!")


@app.task(
    name='MP_SERPENT_lavka',
    retry_kwargs={'max_retries': 5, 'countdown': 5},
    retry_backoff=True,
    autoretry_for=(Exception,),
    queue='lavka',
    ignore_result=True,
)
def task_lavka(task):
    parser = PARSERS[task['screen']][task['marketplace']]
    clickhouse_client = get_clickhouse_client()
    try:
        data = parser.get_and_parse_json(task)
        clickhouse_client.execute(
            "INSERT INTO lavka "
            "(result_id, ts, date_parsed, kw_or_url, shop_address, position, product_id, lavka_brand, "
            "title, link, image_link, price, oldprice, client, category, subcategory, brand, subbrand,"
            " status, stocks, device) VALUES",
            data
        )
    except Exception as e:
        del clickhouse_client
        try:
            del data
        except NameError:
            pass
        raise e
    print("All ok. Finishing...")
    del clickhouse_client
    del data
    del parser
    print("Bye!")


@app.task(
    name='MP_SERPENT_metro',
    retry_kwargs={'max_retries': 5, 'countdown': 5},
    retry_backoff=True,
    autoretry_for=(Exception,),
    queue='metro',
    ignore_result=True,
)
def task_metro(task):
    parser = PARSERS[task['screen']][task['marketplace']]
    print("[F:task_metro] Parser object has been prepared.")
    clickhouse_client = get_clickhouse_client()
    print("[F:task_metro] Clickhouse client has been prepared.")
    try:
        data = parser.get_and_parse_json(task)
        print(f"[F:task_metro] Got data from parser.")
        clickhouse_client.execute(
            "INSERT INTO metro (result_id, ts, date_parsed, kw_or_url, shop_name, shop_address, position, "
            "product_id, product_sku, product_retailer_sku, title, rating, review_count, link, image_link, "
            "sales_points, price, oldprice, client, category, subcategory, brand, subbrand, device) VALUES",
            data
        )
        print(f"[F:task_metro] All data in database. Done.")
    except Exception as e:
        del clickhouse_client
        try:
            del data
        except NameError:
            pass
        raise e
    print("[F:task_metro] All ok. Finishing...")
    del clickhouse_client
    del data
    del parser
    print("[F:task_metro] Bye!")


@app.task(
    name='MP_SERPENT_vprok',
    retry_kwargs={'max_retries': 5, 'countdown': 5},
    retry_backoff=True,
    autoretry_for=(Exception,),
    queue='vprok',
    ignore_result=True,
)
def task_vprok(task):
    parser = PARSERS[task['screen']][task['marketplace']]
    clickhouse_client = get_clickhouse_client()
    try:
        data = parser.get_and_parse_json(task)
        clickhouse_client.execute(
            "INSERT INTO vprok (result_id, ts, date_parsed, kw_or_url, position, product_id, title, "
            "sales_points, rating, link, image_link, price, oldprice, client, "
            "category, subcategory, brand, subbrand, device) VALUES",
            data
        )
    except Exception as e:
        del clickhouse_client
        try:
            del data
        except NameError:
            pass
        raise e
    print("All ok. Finishing...")
    del clickhouse_client
    del data
    del parser
    print("Bye!")


@app.task(
    name='MP_SERPENT_sbermarket',
    retry_kwargs={'max_retries': 5, 'countdown': 5},
    retry_backoff=True,
    autoretry_for=(Exception,),
    queue='sbermarket',
    ignore_result=True,
)
def task_sbermarket(task):
    parser = PARSERS[task['screen']][task['marketplace']]
    clickhouse_client = get_clickhouse_client()
    try:
        data = parser.get_and_parse_json(task)
        clickhouse_client.execute(
            "INSERT INTO sbermarket (result_id, ts, date_parsed, kw_or_url, shop_name, shop_address, position, "
            "product_id, product_sku, product_retailer_sku, title, rating, review_count, link, image_link, "
            "sales_points, price, oldprice, client, category, subcategory, brand, subbrand, device) VALUES",
            data
        )
    except Exception as e:
        del clickhouse_client
        try:
            del data
        except NameError:
            pass
        raise e
    print("All ok. Finishing...")
    del clickhouse_client
    del data
    del parser
    print("Bye!")


TASKS = {
    'wildberries': task_wildberries,
    'ozon': task_ozon,
    'lavka': task_lavka,
    'metro': task_metro,
    'vprok': task_vprok,
    'sbermarket': task_sbermarket,
}


@app.task(bind=True, ignore_result=True, name='MP_SERPENT_task_generator', queue='default')
def task_generator(self, *args, **kwargs):
    project = Project.objects.get(uuid=kwargs['project'])
    if not project.is_active:
        print(f"Project '{project.name}' (id: {project.id}) is not active. Finishing task...")
        return
    elif not project.client.is_active:
        print(f"Client '{project.client.name}' (id: {project.client.id}) is not active. Finishing task...")
        return
    marketplace = project.marketplace.tech_name
    if project.screen == "D":
        screens = ["D"]
    elif project.screen == "M":
        screens = ["M"]
    else:
        screens = ["M", "D"]
    kwgs = KeywordGroup.objects.filter(project__uuid=kwargs['project'])
    timestamp = int(datetime.datetime.utcnow().timestamp())
    for screen in screens:

        if not PARSERS[screen].get(marketplace):
            print(f"No parser for '{marketplace}' marketplace. Screen is {screen}.")
            continue

        for kwg in kwgs:
            if not kwg.is_active:
                print(f"Keyword group '{kwg.name}' (id: {kwg.id}) is not active. Finishing task...")
                continue
            for kw in kwg.keywords.all():
                try:
                    shops = [
                        {
                            "name": shop.name,
                            "tech_name": shop.tech_name,
                            "id": shop.shop_id
                        }
                        for shop in project.shops.all()
                    ]
                    addresses = [{
                        'address': ADDRESSES[marketplace][screen].format(
                            country=project.geo.country,
                            city=project.geo.city,
                            street=project.geo.street,
                            house=project.geo.house
                        ),
                        'lon': float(project.geo.lon),
                        'lat': float(project.geo.lat),
                        'shops': shops
                    }]
                    if screen == "D" and marketplace in ["sbermarket", "metro"]:
                        addresses[0]["address_parts"] = [ADDRESS_FORMATS[3].format(
                            street=project.geo.street,
                            house=project.geo.house
                        )]
                except KeyError:
                    addresses = None
                try:
                    task = TASKS[marketplace]
                    task_data = {
                        "id": self.request.id,
                        "value": kw.keyword,
                        "client": kwg.project.client.name,
                        "project": kwg.project.marketplace.name,
                        "category": kwg.project.category,
                        "subcategory": kwg.project.subcategory,
                        "screen": screen,
                        "marketplace": marketplace,
                        "brandshelf": kwg.project.brandshelf,
                        "addresses": addresses,
                        "ts": timestamp,
                        "dictionary": loads(kwg.typings)
                    }
                    print(f"Task data: {task_data}")
                    task.delay(task_data)
                except KeyError:
                    continue
