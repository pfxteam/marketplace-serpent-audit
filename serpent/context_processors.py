from serpent.models.engine import UserProfile


def user_info(request):
    if request.user.is_authenticated:
        profile = UserProfile.objects.get(user=request.user)
        return {
            'user_profile': {
                'client_limit': profile.client_limit,
                'client_usage': profile.get_client_usage(),
                'client_active_usage': profile.get_client_active_usage(),
            }
        }
    else:
        return {
            'user_profile': {
                'limit': 0,
                'usage': 0,
                'active': 0
            }
        }
