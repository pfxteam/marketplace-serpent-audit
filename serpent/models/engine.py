from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save

from serpent.models.serpent import Client


class UserProfile(models.Model):
	"""Profile for User model."""
	user = models.OneToOneField(to=User, on_delete=models.CASCADE, related_name='profile')
	client_limit = models.IntegerField(default=10, help_text="Limit of Client number")

	def __str__(self):
		return self.user.email

	def get_client_usage(self):
		return Client.objects.filter(owner=self.user).distinct().count()

	def get_client_active_usage(self):
		return Client.objects.filter(owner=self.user, is_active=True).distinct().count()


# Callbacks for signals
def create_profile(sender, **kwargs):
	"""
	A callback function that creates the `UserProfile<serpent.models.engine.UserProfile>` object
	and links it with User object that calls a signal.

	:param User sender: Django's User object, that called a signal.
	:param dict kwargs: Kwargs.
	:return: None
	"""
	if kwargs['created']:
		UserProfile.objects.create(user=kwargs['instance'])


# Signals
post_save.connect(create_profile, sender=User)
