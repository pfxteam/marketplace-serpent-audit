import logging
from uuid import uuid4

from django.contrib.auth.models import User
from django.db import models

logger = logging.getLogger('serpent')


class Client(models.Model):
    name = models.CharField(max_length=200, null=False, blank=False, unique=True)
    description = models.CharField(max_length=1_000, null=False, blank=True)
    is_active = models.BooleanField(null=False, blank=False, default=False)
    owner = models.ForeignKey(to=User, on_delete=models.CASCADE, null=False, blank=False, related_name="clients")
    shares = models.ManyToManyField(to=User, blank=True, related_name="shared_clients")
    shares_edit = models.ManyToManyField(to=User, blank=True, related_name="shared_editable_clients")
    created = models.DateTimeField(auto_now_add=True)
    changed = models.DateTimeField(auto_now=True)
    kw_limit = models.IntegerField(default=1_000)

    def __str__(self):
        return self.name

    def get_active_kws(self):
        clients_active_kwgs = KeywordGroup.objects.filter(project__client=self, is_active=True)
        return Keyword.objects.filter(
            keywordgroup__in=clients_active_kwgs
        ).distinct()

    def get_kws(self):
        return Keyword.objects.filter(
            keywordgroup__project__client=self
        ).distinct()


class Geo(models.Model):
    name = models.CharField(max_length=64, null=False, blank=False, unique=True)
    country = models.CharField(max_length=64, null=False, blank=False)
    city = models.CharField(max_length=64, null=False, blank=False)
    street = models.CharField(max_length=64, null=False, blank=False)
    house = models.CharField(max_length=64, null=False, blank=False)
    lon = models.CharField(max_length=64, null=False, blank=False)
    lat = models.CharField(max_length=64, null=False, blank=False)

    def __str__(self):
        return self.name


class Marketplace(models.Model):
    name = models.CharField(max_length=128, blank=False, null=False)
    tech_name = models.CharField(max_length=128, blank=False, null=False)

    def __str__(self):
        return f"{self.tech_name} ({self.name})"


class Shop(models.Model):
    name = models.CharField(max_length=128, null=False, blank=False)
    tech_name = models.CharField(max_length=64, null=False, blank=False)
    shop_id = models.CharField(max_length=128, null=False, blank=False)
    marketplace = models.ForeignKey(to=Marketplace, null=False, blank=False, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name} [{self.marketplace.name} (id: {self.marketplace.id})]"


class Project(models.Model):
    # Screens
    BOTH = 'B'
    MOBILE = 'M'
    DESKTOP = 'D'
    SCREENS = (
        (MOBILE, 'Mobile'),
        (DESKTOP, 'Desktop'),
        (BOTH, 'Desktop & Mobile'),
    )

    uuid = models.CharField(max_length=64, null=False, blank=False, default=uuid4, unique=True)
    name = models.CharField(max_length=200, null=False, blank=False)
    crontab = models.CharField(max_length=64, null=False, blank=False, default="0 * * * *")
    description = models.CharField(max_length=1000, null=False, blank=True)
    is_active = models.BooleanField(null=False, blank=False, default=False)
    category = models.CharField(max_length=256, null=False, blank=False)
    subcategory = models.CharField(max_length=256, null=False, blank=False)
    geo = models.ForeignKey(to=Geo, blank=False, null=False, on_delete=models.CASCADE)
    screen = models.CharField(choices=SCREENS, blank=False, null=False, max_length=1)
    marketplace = models.ForeignKey(to=Marketplace, blank=False, null=False, on_delete=models.CASCADE)
    shops = models.ManyToManyField(to=Shop, blank=True, null=False)
    brandshelf = models.BooleanField(default=False)
    client = models.ForeignKey(to=Client, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    changed = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = (
            ('name', 'client')
        )

    def get_active_kws(self):
        projects_active_kwgs = KeywordGroup.objects.filter(project=self, is_active=True)
        return Keyword.objects.filter(
            keywordgroup__in=projects_active_kwgs
        ).distinct()

    def get_kws(self):
        return Keyword.objects.filter(
            keywordgroup__project=self,
        ).distinct()


class Keyword(models.Model):
    keyword = models.CharField(max_length=255, blank=False, null=False)
    added = models.DateField(auto_now_add=True)
    is_active = models.BooleanField(default=False, blank=False, null=False)

    def __str__(self):
        return self.keyword

    class Meta:
        # unique_together = (
        #     ('keyword', 'geo')
        # )
        indexes = [
            models.Index(fields=['keyword'], name='mp_serpent_keyword_idx'),
        ]


class KeywordGroup(models.Model):
    name = models.CharField(max_length=128, null=False, blank=False)
    description = models.CharField(max_length=1024, null=False, blank=True)
    is_active = models.BooleanField(null=False, blank=False, default=False)
    keywords = models.ManyToManyField(to=Keyword, blank=True)
    typings = models.CharField(max_length=4096, null=False, blank=False, default=dict())
    brand = models.CharField(max_length=128, null=False, blank=True)
    subbrand = models.CharField(max_length=128, null=False, blank=True)
    project = models.ForeignKey(to=Project, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    changed = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def get_count(self):
        """Returns a count of keywords that includes in the group."""
        return self.keywords.count()

    def delete(self, using=None, keep_parents=False):
        self.keywords.clear()
        super().delete(using, keep_parents)
        Keyword.objects.filter(keywordgroup=None).delete()
